from telegram import *
from telegram.ext import *
from mongoDB import *
from campaignState import *


# Deletes the campaign created on the group where this command is called
# Only GM can call this command
def delete_campaign(update: Update, context: CallbackContext):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Provando a eliminare la campagna...")

    # Gets the Group from DB
    group = get_group_collection().find_one(
        {"telegram-id": update.message.chat_id}
    )

    # Check if the group is registered in the DB (therefore if there is a campaign to delete)
    if group is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non c'è nessuna campagna da eliminare")
        return

    current_campaign = group["current-campaign"]

    # Check that who called the command is the GM
    if is_not_master_by_tid(update.message.from_user.id, current_campaign):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    # Deletes players and their survivors
    for playerId in get_campaign_collection().find_one({"_id": current_campaign})["players"]:
        get_player_collection().find_one_and_update(
            {"_id": playerId}, {"$unset": {"campaign-id": 1}})
        get_survivor_collection().delete_one({"player-id": playerId})
    game_master_id = get_campaign_collection().find_one({"_id": current_campaign})["game-master"]
    get_player_collection().find_one_and_update(
        {"_id": game_master_id}, {"$unset": {"campaign-id": 1}})

    # Deletes the PNGs
    pngs = get_campaign_collection().find_one({"_id": current_campaign})["png"]
    for png in pngs:
        get_png_collection().delete_one({"_id": png})

    # Deletes the campaign, the enclave and the group
    get_enclave_collection().delete_one({"campaign-id": current_campaign})
    get_campaign_collection().delete_one({"_id": current_campaign})
    get_group_collection().delete_one({"current-campaign": current_campaign})

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="campagna eliminata con successo")


# Creates a new campaign, sets up base parameters in the DB
# Whoever calls this command will be registered as the GM
def create_campaign(update: Update, context: CallbackContext):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Sto creando la campagna...")

    # Gets the Group from DB
    group = get_group_collection().find_one(
        {"telegram-id": update.message.chat_id}
    )

    # Check if the group is already registered in the DB (only one campaign per group can be created)
    if group is not None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Creazione fallita!\nPuò essere attiva una sola campagna per gruppo.")
        return

    # Automatically adds the player to the DB if they never had an interaction with the bot
    player = get_player_collection().find_one(
        {"telegram-id": update.message.from_user.id})

    if player is None:
        player_id = get_player_collection().insert_one(
            {"telegram-id": update.message.from_user.id, "name": update.message.from_user.username}).inserted_id
    else:
        player_id = player["_id"]

    # Check if the user is in another campaign
    if "campaign" in player:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Sei già in una campagna, non puoi crearne un altra!")

    # Creates the campaign and sets the GM id
    result = get_campaign_collection().insert_one(
        {"game-master": player_id, "date": update.message.date, "players": [], "state": State.INIT.value,
         "creazione-rapporto": [], "png": []}).inserted_id

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Campagna creata! codice della campagna: <code>" +
                                               str(result) + "</code>",
        parse_mode=ParseMode.HTML)

    # Creates the group
    get_group_collection().insert_one(
        {"telegram-id": update.message.chat_id, "current-campaign": result}
    )

    # Sets the campaign id on the GM
    get_player_collection().find_one_and_update(
        {"_id": player_id}, {"$set": {"campaign-id": result}})



# Adds a player to the campaign (or multiple)
# Needs to be already inserted into the DB (must have interacted with the bot at least once)
def add_player_to_campaign(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi inserire uno @username dopo /add ...")
        return

    # Does some initial checks
    add_boolean, group_id, campaign_id = initial_checks(update, context)
    if not add_boolean:
        return

    # Add each user passed as parameters and checks if they can be added
    for user in context.args:
        if user[0] != "@":
            context.bot.send_message(
                chat_id=update.effective_chat.id, text=user + " non è un utente valido")
            continue

        user = user.removeprefix("@")

        if is_master_by_name(user, campaign_id):
            context.bot.send_message(
                chat_id=update.effective_chat.id, text=user + ", Il Game Master non può giocare ad una campagna")
            continue

        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Aggiungendo " + user + " ...")

        player = get_player_collection().find_one(
            {"name": user})

        if player is None:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non posso aggiungere " +
                                                       user + ", deve prima iniziare una conversazione con me in "
                                                              "privato!")
            continue

        if player["_id"] in get_campaign_collection().find_one({"_id": campaign_id})["players"]:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Il Giocatore è già stato aggiunto")
            continue

        if "campaign-id" in player and player["campaign-id"] != campaign_id:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Il giocatore è già all'interno di una campagna")
            continue

        size = len(get_campaign_collection().find_one(
            {"_id": campaign_id})["players"])

        if size >= 8:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non posso aggiungere " +
                                                       user + ", la lobby è già piena!")
            continue

        get_player_collection().find_one_and_update(
            {"_id": player["_id"]}, {"$set": {"campaign-id": campaign_id}})
        get_campaign_collection().update_one(
            {"_id": campaign_id}, {"$push": {"players": player["_id"]}}
        )

        context.bot.send_message(
            chat_id=update.effective_chat.id, text=user + " aggiunto con successo.")


# Removes a player from the campaign (or multiple)
# Needs to be already inserted into the DB (must have interacted with the bot at least once)
def remove_player(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi inserire uno @username dopo /remove ...")
        return

    # Does some initial checks
    remove_boolean, group_id, campaign_id = initial_checks(update, context)
    if not remove_boolean:
        return

    # Remove each user passed as parameters and checks if they can be Removed
    for user in context.args:
        if user[0] != "@":
            context.bot.send_message(
                chat_id=update.effective_chat.id, text=user + " non è un utente valido")
            continue

        user = user.removeprefix("@")

        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Sto rimuovendo " + user + " ...")

        player = get_player_collection().find_one(
            {"name": user})

        if player is None:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non posso rimuovere " +
                                                       user + ", deve prima iniziare una conversazione con me in "
                                                              "privato!")
            continue

        size = len(get_campaign_collection().find_one(
            {"_id": campaign_id})["players"])

        if size <= 2:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="ATTENZIONE! se rimuovi " +
                                                       user + ", potrebbero non esserci abbastanza giocatori!")

        get_campaign_collection().find_one_and_update(
            {"_id": campaign_id}, {"$pull": {"players": player["_id"]}}
        )

        if len(get_campaign_collection().find_one({"_id": campaign_id})["players"]) == size:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text=user + " non trovato")
            continue

        get_player_collection().find_one_and_update(
            {"_id": player["_id"]}, {"$unset": {"campaign-id": 1}})
        get_survivor_collection().delete_one({"player-id": player["_id"]})

        context.bot.send_message(
            chat_id=update.effective_chat.id, text=user + " rimosso con successo")


# Does some initial checks
def initial_checks(update: Update, context: CallbackContext):
    group_id = get_group_collection().find_one(
        {"telegram-id": update.message.chat_id}
    )

    # Check if there is a campaign in the group
    if group_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna!")
        return False, None, None

    campaign_id = find_campaign_by_chat_id(update.message.chat_id)

    # Check if the state is correct
    if get_state(campaign_id) != State.INIT.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return False, None, None

    # Check if the master run the command
    if is_not_master_by_tid(update.message.from_user.id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return False, None, None

    return True, group_id, campaign_id
