from re import A
import telegram
from bson import ObjectId
from telegram import *
from telegram.ext import *
from mongoDB import *
from campaignState import *
from library import *
import resources


# Handles callbacks from inline keyboard queries
def button(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    query.answer()
    chat_id = context._chat_id_and_data[0]
    if chat_id < 0:
        campaign_id = find_campaign_by_chat_id(chat_id)
    else:
        campaign_id = get_campaign_id_from_user_tid(chat_id)
    enclave = get_enclave_collection().find_one({"campaign-id": campaign_id})

    if campaign_id is None or get_state(campaign_id) == State.INIT.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    # Each response contains the query.data, which is structured like this:
    # " *CallbackID* - *ResponseString* "
    callback_id = query.data.split('-')[0]
    if callback_id == "00":
        if query.data in enclave["carenze"]:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$pull": {"carenze": query.data}}
            )
            query.edit_message_text(text=f"Hai rimosso: {carenze[query.data]}")
        else:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$push": {"carenze": query.data}}
            )
            query.edit_message_text(
                text=f"Hai aggiunto: {carenze[query.data]}")
        return
    elif callback_id == "01":
        if query.data in enclave["vicinanze"]:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$pull": {"vicinanze": query.data}}
            )
            query.edit_message_text(
                text=f"Hai rimosso: {vicinanze[query.data]}")
        else:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$push": {"vicinanze": query.data}}
            )
            query.edit_message_text(
                text=f"Hai aggiunto: {vicinanze[query.data]}")
        return
    elif callback_id == "02":
        if query.data in enclave["abitanti"]:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$pull": {"abitanti": query.data}}
            )
            query.edit_message_text(
                text=f"Hai rimosso: {abitanti[query.data]}")
        else:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$push": {"abitanti": query.data}}
            )
            query.edit_message_text(
                text=f"Hai aggiunto: {abitanti[query.data]}")
        return
    elif callback_id == "03":
        if query.data in enclave["vantaggi"]:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$pull": {"vantaggi": query.data}}
            )
            query.edit_message_text(text=f"Hai rimosso: {query.data.split('-')[1]}")
        else:
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$push": {"vantaggi": query.data}}
            )
            query.edit_message_text(text=f"Hai aggiunto: {query.data.split('-')[1]}")
            image_registry = resources.get_images_by_tag(
                f"{vantaggi[query.data]}")
            images = []
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=update.effective_chat.id, media=images)
        return
    elif callback_id == "06":
        user_tid = chat_id
        player_id = get_player_collection().find_one(
            {"telegram-id": user_tid}
        )["_id"]
        survivor = get_survivor_collection().find_one(
            {"player-id": player_id}
        )
        get_survivor_collection().find_one_and_update(
            {"player-id": player_id}, {"$push": {"presente": query.data}}
        )
        if survivor["sopravvivenza"] != 0:
            complete_survivor_creation(player_id, user_tid, context)
        query.edit_message_text(text=f"Hai scelto: {query.data.split('-')[1]}")
        return
    elif callback_id == "07":
        user_tid = chat_id
        player_id = get_player_collection().find_one(
            {"telegram-id": user_tid}
        )["_id"]
        survivor = get_survivor_collection().find_one(
            {"player-id": player_id}
        )
        if survivor["ferocia"] == 0:
            get_survivor_collection().find_one_and_update(
                {"player-id": player_id}, {"$set": {"ferocia": statistiche[query.data]}}
            )
        elif survivor["fermezza"] == 0:
            get_survivor_collection().find_one_and_update(
                {"player-id": player_id}, {"$set": {"fermezza": statistiche[query.data]}}
            )
        elif survivor["anima"] == 0:
            get_survivor_collection().find_one_and_update(
                {"player-id": player_id}, {"$set": {"anima": statistiche[query.data]}}
            )
        else:
            get_survivor_collection().find_one_and_update(
                {"player-id": player_id}, {"$set": {"sopravvivenza": statistiche[query.data]}}
            )
            query.edit_message_text(text="Hai Completato la selezione\nLe tue statistiche sono:\n1° "
                                         f"Ferocia: {survivor['ferocia']};\n2° Fermezza: {survivor['fermezza']};\n3° "
                                         f"Anima: {survivor['anima']};\n4° sopravvivenza: {statistiche[query.data]}")
            if len(survivor["presente"]) > 0:
                complete_survivor_creation(player_id, user_tid, context)
        return
    elif callback_id == "08":
        user_tid = query.from_user.id
        if is_not_master_by_tid(user_tid, campaign_id):
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
            return

        if len(get_campaign_collection().find_one({"_id": campaign_id})["creazione-rapporto"]) == 0:
            return

        players = get_campaign_collection().find_one({"_id": campaign_id})["creazione-rapporto"]
        player1 = players[0]
        player2 = players[1]

        get_survivor_collection().find_one_and_update({"campaign-id": campaign_id, "player-id": player1},
                                                      {"$push": {"rapporto": {"other": player2, "what": query.data}}})
        get_survivor_collection().find_one_and_update({"campaign-id": campaign_id, "player-id": player2},
                                                      {"$push": {"rapporto": {"other": player1, "what": query.data}}})

        get_campaign_collection().find_one_and_update({"_id": campaign_id}, {"$set": {"creazione-rapporto": []}})

        query.edit_message_text(
            text=f"Ho aggiunto il rapporto: \"{query.data.split('-')[1]}\"")
        return
    elif callback_id == "R08":
        user_tid = query.from_user.id
        # this specific query data structure is: " R *CallbackID* - *player_id* | *ResponseString* "
        player_id = ObjectId(query.data.split('-')[1].split('|')[0])
        query_data = '08-' + query.data.split('-')[1].split('|')[1]

        if is_not_master_by_tid(user_tid, campaign_id):
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
            return

        survivor = get_survivor_collection().find_one({"player-id": player_id})
        relationships = survivor["rapporto"]
        player2_id = None
        found = False
        for element in relationships:
            if element["what"] == query_data:
                found = True
                player2_id = element["other"]
                break

        if not found:
            query.edit_message_text(
                text=f"Il rapporto è già stato eliminato precedentemente")
            return

        get_survivor_collection().find_one_and_update({"campaign-id": campaign_id, "player-id": player_id},
                                                      {"$pull": {
                                                          "rapporto": {"what": query_data, "other": player2_id}}})
        get_survivor_collection().find_one_and_update({"campaign-id": campaign_id, "player-id": player2_id},
                                                      {"$pull": {
                                                          "rapporto": {"what": query_data, "other": player_id}}})

        query.edit_message_text(
            text=f"Ho rimosso il rapporto: \"{query_data.split('-')[1]}\"")
        return
    elif callback_id == "09":
        group_chat_id = get_group_collection().find_one({"current-campaign": campaign_id})["telegram-id"]
        pngs_in_campaign = get_png_in_campaign(campaign_id)

        if png_in_campaign(query.data, pngs_in_campaign):
            deleted_id = get_png_collection().find_one({"campaign-id": campaign_id, "name": query.data})["_id"]
            get_png_collection().delete_one({"campaign-id": campaign_id, "name": query.data})
            get_campaign_collection().find_one_and_update(
                {"_id": campaign_id}, {"$pull": {"png": deleted_id}}
            )
            query.edit_message_text(
                text=f"Ho rimosso {query.data.split('-')[1]}")

            context.bot.send_message(
                chat_id=group_chat_id,
                text=f"{query.data.split('-')[1]} è morto e verrà rimosso dalla campagna")
        else:
            inserted_png_id = get_png_collection().insert_one(
                {"campaign-id": campaign_id, "name": query.data, "stress": 0, "allies": []}
            ).inserted_id

            get_campaign_collection().find_one_and_update(
                {"_id": campaign_id}, {"$push": {"png": inserted_png_id}}
            )

            query.edit_message_text(
                text=f"Ho aggiunto il nuovo PNG: {query.data.split('-')[1]}")

            image_registry = resources.get_images_by_precise_tag(
                f"{personaggi[query.data]}")
            images = []
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=update.effective_chat.id, media=images)

            context.bot.send_message(
                chat_id=group_chat_id,
                text=f"Il GM ha creato un nuovo superstite: {query.data.split('-')[1]}")

        return


# Used to simplify survivor creation
def complete_survivor_creation(player_id, player_tid, context):
    campaign_id = get_campaign_id_from_user_tid(player_tid)
    group_chat_id = get_group_collection().find_one({"current-campaign": campaign_id})["telegram-id"]
    player_name = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["name"]
    context.bot.send_message(
        chat_id=group_chat_id,
        text=player_name + " ha creato il suo superstite!")
    get_survivor_collection().find_one_and_update(
        {"player-id": player_id}, {"$set": {"creation-done": True}}
    )


# Keyboard of carenze
def keyboard_carenze(update: Update, context: CallbackContext):
    keyboard = [
        [
            InlineKeyboardButton("Cibo", callback_data='00-cibo'),
            InlineKeyboardButton("Medicinali", callback_data='00-medicinali'),
        ],
        [
            InlineKeyboardButton("Privacy", callback_data='00-privacy'),
            InlineKeyboardButton("Sicurezza", callback_data='00-sicurezza'),
        ],
        [
            InlineKeyboardButton("Armi", callback_data='00-armi'),
            InlineKeyboardButton("Comodità", callback_data='00-comodità'),
        ]
    ]
    update.message.reply_text(
        'Scegli una carenza da modificare:\n(Se era già presente verrà rimossa, altrimenti verrà aggiunta)',
        reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of vicinanze
def keyboard_vicinanze(update: Update, context: CallbackContext):
    campaign_id = find_campaign_by_chat_id(update.message.chat_id)
    enclave = get_enclave_collection().find_one({"campaign-id": campaign_id})
    if enclave["enclave-type"] == "prigione":
        keyboard = [
            [
                InlineKeyboardButton(
                    "Un pericoloso avamposto militare", callback_data='01-avamposto'),
            ],
            [
                InlineKeyboardButton(
                    "Una foresta rigogliosa", callback_data='01-foresta'),
            ],
            [
                InlineKeyboardButton(
                    "Un'enclave abbandonata e non sicura", callback_data='01-enclave'),
            ],
            [
                InlineKeyboardButton(
                    "Un'insuperabile catena montuosa", callback_data='01-montagne'),
            ],
            [
                InlineKeyboardButton(
                    "Un vasto specchio d'acqua", callback_data='01-acqua'),
            ],
            [
                InlineKeyboardButton(
                    "Una grande città brulicante di zombie", callback_data='01-città'),
            ],
        ]
    else:
        keyboard = [
            [
                InlineKeyboardButton(
                    "Un complesso di uffici barricato", callback_data='01-uffici'),
            ],
            [
                InlineKeyboardButton("Una darsena o porto",
                                     callback_data='01-porto'),
            ],
            [
                InlineKeyboardButton(
                    "Un posto di polizia fortificato", callback_data='01-polizia'),
            ],
            [
                InlineKeyboardButton(
                    "Una centrale elettrica", callback_data='01-centrale'),
            ],
            [
                InlineKeyboardButton(
                    "Un grande centro commerciale", callback_data='01-supermercato'),
            ],
            [
                InlineKeyboardButton(
                    "Un quartiere cittadino in rovina", callback_data='01-quartiere'),
            ]
        ]

    update.message.reply_text(
        'Scegli una vicinanza da modificare:\n(Se era già presente verrà rimossa, altrimenti verrà aggiunta)',
        reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of abitanti
def keyboard_abitanti(update: Update, context: CallbackContext):
    campaign_id = find_campaign_by_chat_id(update.message.chat_id)
    enclave = get_enclave_collection().find_one({"campaign-id": campaign_id})
    if enclave["enclave-type"] == "prigione":
        keyboard = [
            [
                InlineKeyboardButton("Un detenuto famoso",
                                     callback_data='02-detenuto'),
            ],
            [
                InlineKeyboardButton(
                    "Una dozzina di profughi dalla città", callback_data='02-profughi'),
            ],
            [
                InlineKeyboardButton(
                    "La famiglia di una fattoria locale", callback_data='02-contadini'),
            ],
            [
                InlineKeyboardButton(
                    "Una coppia di soldati", callback_data='02-soldati'),
            ],
            [
                InlineKeyboardButton(
                    "Il direttore della prigione", callback_data='02-direttore'),
            ],
            [
                InlineKeyboardButton(
                    "Un gruppetto di secondini", callback_data='02-secondini'),
            ],
            [
                InlineKeyboardButton(
                    "Una famiglia della periferia", callback_data='02-famiglia'),
            ],
        ]
    else:
        keyboard = [
            [
                InlineKeyboardButton(
                    "Un gruppo di pazienti superstiti", callback_data='02-pazienti'),
            ],
            [
                InlineKeyboardButton(
                    "Un funzionario cittadino", callback_data='02-funzionario'),
            ],
            [
                InlineKeyboardButton(
                    "Una dozzina di profughi dalla città", callback_data='02-profughi'),
            ],
            [
                InlineKeyboardButton("Un medico esperto",
                                     callback_data='02-medico'),
            ],
            [
                InlineKeyboardButton(
                    "Un amministratore ospedaliero", callback_data='02-amministratore'),
            ],
            [
                InlineKeyboardButton(
                    "Un ufficiale dell'esercito", callback_data='02-ufficiale'),
            ],
            [
                InlineKeyboardButton(
                    "Un gruppetto di poliziotti", callback_data='02-poliziotti'),
            ],
        ]

    update.message.reply_text(
        "Scegli un'abitante da modificare:\n(Se era già presente verrà rimosso, altrimenti verrà aggiunto)",
        reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of vantaggi
def keyboard_vantaggi(update: Update, context: CallbackContext):
    keyboard = [
        [
            InlineKeyboardButton("Elicottero", callback_data='03-elicottero'),
            InlineKeyboardButton("Generatori", callback_data='03-generatori'),
        ],
        [
            InlineKeyboardButton("Quarantena", callback_data='03-quarantena'),
            InlineKeyboardButton("Mensa", callback_data='03-mensa'),
        ],
        [
            InlineKeyboardButton("Rimessa", callback_data='03-rimessa'),
            InlineKeyboardButton("Infermeria", callback_data='03-infermeria'),
        ],
        [
            InlineKeyboardButton("Armeria", callback_data='03-armeria'),
            InlineKeyboardButton("Recinzioni solide",
                                 callback_data='03-recinzioni solide'),
        ]
    ]
    update.message.reply_text(
        'Scegli un vantaggio da modificare:\n(Se era già presente verrà rimosso, altrimenti verrà aggiunto)',
        reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of statistiche
def keyboard_statistiche(update: Update, context: CallbackContext):
    keyboard = [
        [
            InlineKeyboardButton("1", callback_data='07-uno')
        ],
        [
            InlineKeyboardButton("2", callback_data='07-due')
        ],
        [
            InlineKeyboardButton("3", callback_data='07-tre')
        ]
    ]

    update.message.reply_text(
        "Qui invece devi assegnare le tue statistiche.\nScegli quanto assegnare alle statistiche in ordine:\n[1° "
        "Ferocia, 2° Fermezza, 3° Anima, 4° sopravvivenza]\n(è consigliato assegnare un totale di 8 -> 3,2,2,1)",
        reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of rapporto
def keyboard_rapporto(update: Update, context: CallbackContext, entry_set):
    keyboard = [[InlineKeyboardButton(entry.split('-')[1], callback_data=entry)] for entry in entry_set]
    if len(keyboard) == 0:
        update.message.reply_text("Non ci sono tipologie di relazioni rimaste tra questi due player")
    else:
        update.message.reply_text("Scegli il rapporto", reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of remove rapporto
def keyboard_remove_rapporto(update: Update, context: CallbackContext, entry_set, player_id):
    keyboard = []
    for entry in entry_set:
        other_player = player_username_by_id(entry[1])
        keyboard.append([InlineKeyboardButton(other_player + ': ' + entry[0].split('-')[1],
                                              callback_data='R' + entry[0].split('-')[0] + '-' + str(player_id) + '|' +
                                                            entry[0].split('-')[1])])
        # example of callback: "R08-62d587141c25a37cf53c954c|avete una relazione"
    update.message.reply_text("Scegli quale rapporto rimuovere", reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of add_png
def keyboard_add_png(update: Update, context: CallbackContext, remaining_pngs):
    keyboard = [[InlineKeyboardButton(entry[0].split('-')[1], callback_data=entry[0])] for entry in remaining_pngs]
    update.message.reply_text("Scegli il Personaggio da aggiungere", reply_markup=InlineKeyboardMarkup(keyboard))


# Keyboard of removePNG
def keyboard_remove_png(update: Update, context: CallbackContext, remaining_pngs):
    keyboard = [[InlineKeyboardButton(entry[0].split('-')[1], callback_data=entry[0])] for entry in remaining_pngs]
    update.message.reply_text("Scegli il Personaggio da uccidere", reply_markup=InlineKeyboardMarkup(keyboard))
