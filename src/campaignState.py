import enum
from mongoDB import get_campaign_collection


# Enum of the States of the Campaign
class State(enum.Enum):
    INIT = 0
    CREATE_ENCLAVE = 10
    CREATE_SURVIVORS = 20
    CREATE_RELATIONSHIPS = 30
    CREATE_PNGS = 40
    IN_GAME = 50
    ERROR = 100


# Getter for the current State of the campaign
def get_state(campaign_id):
    campaign = get_campaign_collection().find_one(
        {"_id": campaign_id})
    if campaign is None:
        return State.ERROR.value
    return campaign["state"]


# Setter for the State of the campaign
def update_state(campaign_id, new_state: State):
    get_campaign_collection().find_one_and_update(
        {"_id": campaign_id}, {"$set": {"state": new_state.value}})
