import math
import random
import telegram
from telegram import *
from telegram.ext import *
from campaignState import *
from library import *
from inlineKeyBoard import *
from mongoDB import *
from utils import *
import resources


# Increases stress of the user who called the command
def increase_stress(update: Update, context: CallbackContext):
    # Initiate variables containing player and survivor information
    player_tid = update.message.from_user.id
    player_id = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["_id"]
    player_name = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["name"]
    campaign_id = get_campaign_id_from_user_tid(player_tid)
    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if get_state(campaign_id) != State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="devi prima creare un Superstite")
        return

    # When stress reaches 4 it resets and user is notified to draw a trauma card
    if survivor["stress"] == 4:
        get_survivor_collection().find_one_and_update({"player-id": player_id}, {"$set": {"stress": 0}})
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name}, Il tuo stress è diventato troppo alto (5)! ora verrà impostato a 0, sei pregato di "
                 f"effettuare il comando /draw_trauma in privato")
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name}, Il tuo livello di stress si è alzato di uno: {survivor['stress'] + 1}")
        get_survivor_collection().find_one_and_update({"player-id": player_id},
                                                      {"$set": {"stress": survivor['stress'] + 1}})


# Decreases stress of the user who called the command
def decrease_stress(update: Update, context: CallbackContext):
    # Initiate variables containing player and survivor information
    player_tid = update.message.from_user.id
    player_id = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["_id"]
    player_name = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["name"]
    campaign_id = get_campaign_id_from_user_tid(player_tid)
    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if get_state(campaign_id) != State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="devi prima creare un Superstite")
        return

    # Stress level cannot get lower than 0
    if survivor["stress"] == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name}, Il tuo livello di stress è già al minimo (0)")
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name}, Il tuo livello di stress è diminuito di uno: {survivor['stress'] - 1}")
        get_survivor_collection().find_one_and_update({"player-id": player_id},
                                                      {"$set": {"stress": survivor['stress'] - 1}})


# Increases PNG stress level by 1
def increase_png_stress(update: Update, context: CallbackContext):
    player_id = update.message.from_user.id
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)
    campaign = get_campaign_collection().find_one({"_id": campaign_id})

    # Initial checks of availability of the command
    if is_not_master_by_tid(player_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if campaign_id is not None and get_state(campaign_id) != State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if len(context.args) == 0 or len(context.args[0]) < 3:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Iniziali troppo corte, sei pregato di usare almeno 3 lettere per riconoscere il PNG")
        return

    # Searches the PNG with the closes name and augment its stress (min char length is 3)
    for pngID in campaign["png"]:
        png = get_png_collection().find_one({"_id": pngID})
        png_name = str(png["name"])
        if png_name.lower().split('-')[1].startswith(context.args[0].lower()):
            if png["stress"] + 1 == 3:
                context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"{png_name.split('-')[1]} è impazzito a causa dello stress. Il GM è pregato di effettuare"
                         f" /uccidiPNG in privato")
                return
            else:
                get_png_collection().find_one_and_update({"_id": png["_id"]}, {"$set": {"stress": png["stress"] + 1}})
                context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Il livello di stress di {png_name.split('-')[1]} è aumentato di 1 ({png['stress'] + 1})")
                return

    # there are no PNGs which starts with context.args[0]
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Non ho trovato nessun PNG chiamato/che inizia per {context.args[0]}")


# Shows your relationship with the group, in the form: "otherPerson: relatinship"
def show_rapporti(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)

    if get_state(campaign_id) < State.CREATE_SURVIVORS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if len(context.args) > 1:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Per usare il comando devi usare la seguente sintassi:"
                 " /show_rel @player1 oppure /show_rel (l'ultimo mostra i rapporti del player chiamante)")
        return
    elif len(context.args) == 1:
        name1 = context.args[0][1::]
        player_id = player_id_by_username(name1)

    players_in_campaign = get_players_in_campaign(campaign_id)

    if player_id is None or player_id not in players_in_campaign:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non esiste o non è presente nella campagna")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non possiede un survivor vivo al momento")
        return

    if len(survivor["rapporto"]) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non ha ancora alcun rapporto con altri player")
        return

    relationships = survivor["rapporto"]
    response = "I rapporti di *" + normalize_username(player_username_by_id(player_id)) + "* sono:"
    for entry in relationships:
        other_player = normalize_username(player_username_by_id(entry["other"]))
        response = response + '\n' + 'con *' + other_player + '*' + ': ' + entry["what"].split('-')[1]

    update.message.reply_markdown_v2(
        text=f"{response}",
    )


# Shows your past card/cards to a player passed as parameter
# If there is no player in passed parameters (only /showPast), it will show it to everyone in the group
def show_past(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    player2_tid = None
    player2_name = None
    campaign_id = get_campaign_id_from_user_tid(update.message.from_user.id)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non stai partecipando ad una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    passato_cards = survivor["passato"]
    if len(passato_cards) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non hai ancora estratto una carta passato")
        return

    players_in_campaign = get_players_in_campaign(campaign_id)
    player_name = player_username_by_id(player_id)

    if len(context.args) > 1:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Per usare il comando devi usare la seguente sintassi:"
                 " /show_past @player (player = giocatore a cui vuoi mostrare il passato)."
                 "\nSe non metti alcun @player, verrà mostrato a tutti nel gruppo")

        return
    elif len(context.args) == 1:
        param_found = True
        name2 = context.args[0][1::]
        player2_id = player_id_by_username(name2)
        player2_tid = player_tid_by_id(player2_id)
        player2_name = player_username_by_id(player2_id)
        gm_id = gm_id_by_campaign_id(campaign_id)

        if player_id is None or player_id not in players_in_campaign \
                or player2_id is None or (player2_id not in players_in_campaign and player2_id != gm_id):
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"il player inserito non esiste o non è presente nella campagna")
            return
    else:
        param_found = False
        if player_id is None or player_id not in players_in_campaign:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"il player inserito non esiste o non è presente nella campagna")
            return

    images = []
    for tag in passato_cards:
        image_registry = resources.get_images_by_precise_tag(passato[tag])
        for photo_name, photo in image_registry.items():
            images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    if param_found:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name} ha rivelato il suo Passato a {player2_name}")
        context.bot.send_message(
            chat_id=player2_tid,
            text=f"{player_name} ti rivela il suo Passato:")
        context.bot.send_media_group(
            chat_id=player2_tid, media=images)
    else:
        get_survivor_collection().find_one_and_update({"player-id": player_id}, {"$set": {"is-passato-revealed": True}})
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name} rivela il suo Passato:")
        context.bot.send_media_group(
            chat_id=group_tid, media=images)


# Shows your trauma card/cards to a player passed as parameter
def show_trauma(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    player2_tid = None
    player2_name = None
    campaign_id = get_campaign_id_from_user_tid(update.message.from_user.id)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non stai partecipando ad una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    trauma_cards = survivor["trauma"]
    if len(trauma_cards) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non hai ancora estratto una carta trauma")
        return

    players_in_campaign = get_players_in_campaign(campaign_id)
    player_name = player_username_by_id(player_id)

    if len(context.args) > 1:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Per usare il comando devi usare la seguente sintassi:"
                 " /show_trauma @player (player = giocatore a cui vuoi mostrare il passato)."
                 "\nSe non metti alcun @player, verrà mostrato a tutti nel gruppo")

        return
    elif len(context.args) == 1:
        param_found = True
        name2 = context.args[0][1::]
        player2_id = player_id_by_username(name2)
        player2_tid = player_tid_by_id(player2_id)
        player2_name = player_username_by_id(player2_id)
        gm_id = gm_id_by_campaign_id(campaign_id)

        if player_id is None or player_id not in players_in_campaign \
                or player2_id is None or (player2_id not in players_in_campaign and player2_id != gm_id):
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"il player inserito non esiste o non è presente nella campagna")
            return
    else:
        param_found = False
        if player_id is None or player_id not in players_in_campaign:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"il player inserito non esiste o non è presente nella campagna")
            return

    images = []
    for tag in trauma_cards:
        image_registry = resources.get_images_by_precise_tag(trauma[tag])
        for photo_name, photo in image_registry.items():
            images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    if param_found:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name} ha rivelato i suoi traumi a {player2_name}")
        context.bot.send_message(
            chat_id=player2_tid,
            text=f"{player_name} ti rivela i suoi traumi:")
        context.bot.send_media_group(
            chat_id=player2_tid, media=images)
    else:
        get_survivor_collection().find_one_and_update({"player-id": player_id}, {"$set": {"is-trauma-revealed": True}})
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"{player_name} rivela i suoi traumi:")
        context.bot.send_media_group(
            chat_id=group_tid, media=images)


# Shows the enclave
def show_enclave(update: Update, context: CallbackContext):
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.CREATE_SURVIVORS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    enclave = get_enclave_collection().find_one({"campaign-id": campaign_id})

    if enclave is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Erroe: l'enclave è stato rimosso")
        return

    response = "Ecco l'*ENCLAVE*: "
    response = response + '\n' + 'Tipo di enclave: *' + enclave["enclave-type"] + '*'

    response = response + '\n\n' + 'Carenze: '
    if len(enclave["carenze"]) > 0:
        for entry in enclave["carenze"]:
            response = response + '\n' + '  • ' + '*' + carenze[entry] + '*'
    else:
        response = response + '\n' + '  • ' + '*Nessuna carenza*'

    response = response + '\n\n' + 'Vicinanze: '
    if len(enclave["vicinanze"]) > 0:
        for entry in enclave["vicinanze"]:
            response = response + '\n' + '  • ' + '*' + vicinanze[entry] + '*'
    else:
        response = response + '\n' + '  • ' + '*Nessuna vicinanza*'

    response = response + '\n\n' + 'Tipo di abitanti: '
    if len(enclave["abitanti"]) > 0:
        for entry in enclave["abitanti"]:
            response = response + '\n' + '  • ' + '*' + abitanti[entry] + '*'
    else:
        response = response + '\n' + '  • ' + '*Nessun abitante*'

    response = response + '\n\n' + 'Vantaggi: '
    update.message.reply_markdown_v2(
        text=f"{response}",
    )

    if len(enclave["vantaggi"]) > 0:
        images = []
        for tag in enclave["vantaggi"]:
            image_registry = resources.get_images_by_precise_tag(vantaggi[tag])
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
        context.bot.send_media_group(
            chat_id=group_tid, media=images)
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Nessun vantaggio.")


# Shows the survivor of a player passed as parameter. Trauma and past are hidden.
def show_survivor(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})
    player_name = player_username_by_id(player_id)

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    response = "Ecco il *survivor* di *" + normalize_username(player_name) + "*: "
    response = response + '\n' + 'Statistiche: '
    response = response + '\n' + '  • Ferocia: ' + '*' + str(survivor["ferocia"]) + '*'
    response = response + '\n' + '  • Fermezza: ' + '*' + str(survivor["fermezza"]) + '*'
    response = response + '\n' + '  • Anima: ' + '*' + str(survivor["anima"]) + '*'
    response = response + '\n' + '  • Sopravvivenza: ' + '*' + str(survivor["sopravvivenza"]) + '*'
    response = response + '\n\n' + 'Livello di *Stress: ' + str(survivor["stress"]) + '*'
    response = response + '\n\n' + 'Traumi: '
    if "is-trauma-revealed" in survivor:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=f"{response}",
            parse_mode='MarkdownV2')
        response = ""
        if len(survivor["trauma"]) > 0:
            images = []
            for tag in survivor["trauma"]:
                image_registry = resources.get_images_by_precise_tag(trauma[tag])
                for photo_name, photo in image_registry.items():
                    images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=group_tid, media=images)
        else:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non hai nessun trauma")
    else:
        response = response + '\n' + '  • ' + '*Nascosto*, \(x' + str(len(survivor["trauma"])) + " carte\)"
    response = response + '\n\n' + 'Passato: '
    if "is-passato-revealed" in survivor:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=f"{response}",
            parse_mode='MarkdownV2')
        response = ""
        if len(survivor["passato"]) > 0:
            images = []
            for tag in survivor["passato"]:
                image_registry = resources.get_images_by_precise_tag(passato[tag])
                for photo_name, photo in image_registry.items():
                    images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=group_tid, media=images)
        else:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Una leggenda senza passato")
    else:
        response = response + '\n' + '  • ' + '*Nascosto*'
    response = response + '\n\n' + 'Presente: '
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=f"{response}",
        parse_mode='MarkdownV2')

    if len(survivor["presente"]) > 0:
        images = []
        for tag in survivor["presente"]:
            image_registry = resources.get_images_by_precise_tag(presente[tag])
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
        context.bot.send_media_group(
            chat_id=group_tid, media=images)
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Presente non presente?")


# Shows the PNG selected by passed parameter.
def show_png(update: Update, context: CallbackContext):
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)
    campaign = get_campaign_collection().find_one({"_id": campaign_id})

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if len(context.args) == 0 or len(context.args[0]) < 3:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Iniziali troppo corte, sei pregato di usare almeno 3 lettere per riconoscere il PNG")
        return

    # Searches the PNG with the closes name and augment its stress (min char length is 3)
    for pngID in campaign["png"]:
        png = get_png_collection().find_one({"_id": pngID})
        png_name = str(png["name"])
        if png_name.lower().split('-')[1].startswith(context.args[0].lower()):
            response = "Ecco il *PNG* di nome *" + normalize_username(png_name.split('-')[1]) + "*: "
            response = response + '\n' + 'Livello di *Stress: ' + str(png["stress"]) + '*'
            response = response + '\n\n' + '*Alleati*:'
            if len(png["allies"]) == 0:
                response = response + '\n' + '  • ' + '*Nessuno*'
            else:
                for ally_id in png["allies"]:
                    ally_name = player_username_by_id(ally_id)
                    response = response + '\n' + '  • ' + '*' + normalize_username(ally_name) + '*'
            update.message.reply_markdown_v2(
                text=f"{response}",
            )
            images = []
            image_registry = resources.get_images_by_precise_tag(personaggi[png_name])
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=group_tid, media=images)
            return

    # there are no PNGs which starts with context.args[0]
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Non ho trovato nessun PNG chiamato/che inizia per {context.args[0]}")


# Shows PNGs names in campaign
def show_pngs(update: Update, context: CallbackContext):
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    text = "PNG nell'attuale campagna:\n"
    for png in get_pngs_in_campaign(campaign_id):
        text += " • " + png + "\n"

    context.bot.send_message(
        chat_id=update.effective_chat.id, text=text)


# Shows the image that sums up wich base move activators there are
def show_base_moves_activators(update: Update, context: CallbackContext):
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ecco la plancia di aiuto per le MOSSE BASE:")
    images = []
    image_registry = resources.get_images_by_precise_tag("Plancia_7")
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))
    context.bot.send_media_group(
        chat_id=group_tid, media=images)


# Draws a card from the survivor deck
# The command syntax is /draw_survivor *type of attribute indicated by the move activator* *ulterior optional modifiers*
# Examples: -/draw_survivor ferocia +1 -/draw_survivor Anima -/draw_survivor fermezza -1
def draw_from_survivor_deck(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})
    player_name = player_username_by_id(player_id)
    modifier = 0

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    if len(context.args) < 1 or len(context.args) > 2:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non hai specificato con che caratteristicha pescare. "
                 f"\nSi prega di inserire una caratteristica valita tra fermezza,"
                 f" anima, ferocia e sopravvivenza come ad esempio:"
                 f"\n/draw_survivor Anima"
                 f"\nNel caso di modificatori, strutturare la richiesta come segue:"
                 f"\n /draw_survivor Anima +1")
        return
    elif len(context.args) == 2:
        if not is_a_number(context.args[1]):
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"Il modificatore inserito non è nel formato corretto."
                     f" Si prega di inserire un numero come secondo parametro.")
            return
        else:
            modifier = int(context.args[1])
            if modifier > 5 or modifier < -5:
                context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Il modificatore inserito supera il limite consentito."
                         f" Si prega di inserire un numero compreso tra -5 e +5")
                return

    possible_choices = {"fermezza", "anima", "ferocia", "sopravvivenza"}
    param1 = context.args[0].lower()
    if param1 not in possible_choices:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"La caratteristica selezionata non esiste."
                 f" Si prega di inserire una caratteristica valita tra fermezza, anima, ferocia e sopravvivenza")
        return

    if survivor[f"{param1}"] is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non hai ancora assegnato le tue statistiche")
        return

    number_of_draws = int(survivor[f"{param1}"]) + modifier

    if number_of_draws <= 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Il modificatore ha ridotto il numero di carte pescate a 0.")
        return

    outcomes_distribution = [1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4]
    images = []

    for i in range(number_of_draws):
        extracted = random.choice(outcomes_distribution)
        outcomes_distribution.remove(extracted)
        image_registry = resources.get_images_by_precise_tag("Superstiti_" + str(extracted))
        for photo_name, photo in image_registry.items():
            images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"{normalize_username(player_name)} Ha pescato dal mazzo superstiti " + str(number_of_draws) + " cart" + (
            'a' if number_of_draws == 1 else 'e') + ":")
    context.bot.send_media_group(
        chat_id=group_tid, media=images)


# Shows the image that sums up wich base move activators there are
def show_zombie_moves_activators(update: Update, context: CallbackContext):
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ecco la plancia di aiuto per le MOSSE ZOMBIE:")
    images = []
    image_registry = resources.get_images_by_precise_tag("Plancia_6")
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))
    context.bot.send_media_group(
        chat_id=group_tid, media=images)


# Shows GameMaster's helper tablet
def show_gm_moves(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if is_not_master_by_tid(player_tid, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando.\nNon sei il Game Master")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ecco la plancia di aiuto per il GM:")

    images = []
    image_registry = resources.get_images_by_precise_tag("Plancia_0")
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=player_tid, media=images)


# Shows Enclave helper tablet
def show_enclave_helper(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ecco la plancia di aiuto dell'enclave:")

    images = []
    image_registry = resources.get_images_by_precise_tag("Plancia_3")
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=player_tid, media=images)


# Shows Survivor helper tablet
def show_survivor_helper(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ecco la plancia di aiuto del superstite:")

    images = []
    image_registry = resources.get_images_by_precise_tag("Plancia_5")
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=player_tid, media=images)


# Draws a card from the zombie deck
# The command syntax is /draw_zombie *ulterior optional modifiers*
# Examples: -/draw_zombie 1, -/draw_zombie +5, base /draw_zombie == /draw_zombie 1
def draw_from_zombie_deck(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})
    player_name = player_username_by_id(player_id)
    modifier = 1

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    if len(context.args) == 1:
        if not is_a_number(context.args[0]):
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"Il modificatore inserito non è nel formato corretto."
                     f" Si prega di inserire un numero come parametro.")
            return
        else:
            modifier = int(context.args[0])
            if modifier > 9 or modifier < 1:
                context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Il modificatore inserito supera il limite consentito."
                         f" Si prega di inserire un numero compreso tra 1 e 9")
                return

    outcomes_distribution = [1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 5, 5, 5, 5, 5]

    if not is_zombie_deck_present(campaign_id):
        # Creates the deck
        get_zombie_deck_collection().insert_one(
            {"campaign-id": campaign_id, "deck": outcomes_distribution}
        )

    images = []
    deck = get_zombie_deck_collection().find_one({"campaign-id": campaign_id})["deck"]

    for i in range(modifier):
        extracted = random.choice(deck)
        deck.remove(extracted)
        if extracted == 4:
            reset_zombie_deck(campaign_id)
            deck = get_zombie_deck_collection().find_one({"campaign-id": campaign_id})["deck"]
        image_registry = resources.get_images_by_precise_tag("Morso_" + str(extracted))
        for photo_name, photo in image_registry.items():
            images.append(telegram.InputMediaPhoto(open(photo, "rb")))
    set_zombie_deck(campaign_id, deck)

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"{normalize_username(player_name)} Ha pescato dal mazzo morso " + str(modifier) + " cart" + (
            'a' if modifier == 1 else 'e') + ":")
    context.bot.send_media_group(
        chat_id=group_tid, media=images)


# Makes the caller of the command an ally of the png passed as parameter
def make_ally(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)
    campaign = get_campaign_collection().find_one({"_id": campaign_id})

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})
    player_name = player_username_by_id(player_id)

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    if len(context.args) == 0 or len(context.args[0]) < 3:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Iniziali troppo corte, sei pregato di usare almeno 3 lettere per riconoscere il PNG")
        return

    # Searches the PNG with the closes name and augment its stress (min char length is 3)
    for pngID in campaign["png"]:
        png = get_png_collection().find_one({"_id": pngID})
        png_name = str(png["name"])
        if png_name.lower().split('-')[1].startswith(context.args[0].lower()):
            get_png_collection().find_one_and_update({"_id": pngID}, {"$push": {"allies": player_id}})
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"{player_name} è diventato alleato di {png_name.split('-')[1]}")
            return

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"PNG non trovato. Inserire un nome valido")


# Draws a card from the Fate deck
# The command syntax is /draw_fato *ulterior optional modifiers*
# Examples: -/draw_fato 1, -/draw_fato +5, base /draw_fato == /draw_fato 1
def draw_from_fate_deck(update: Update, context: CallbackContext):
    player_id = player_id_by_tid(update.message.from_user.id)
    group_tid = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_tid)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Devi prima creare una campagna.")
        return

    if get_state(campaign_id) < State.IN_GAME.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})
    player_name = player_username_by_id(player_id)
    modifier = 1

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Non possiedi un survivor vivo al momento")
        return

    if len(context.args) == 1:
        if not is_a_number(context.args[0]):
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"Il modificatore inserito non è nel formato corretto."
                     f" Si prega di inserire un numero come parametro.")
            return
        else:
            modifier = int(context.args[0])
            if modifier > 8 or modifier < 1:
                context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Il modificatore inserito supera il limite consentito."
                         f" Si prega di inserire un numero compreso tra 1 e 8")
                return

    outcomes_distribution = [1, 2, 3, 4, 5, 6, 7, 8]
    images = []

    for i in range(modifier):
        extracted = random.choice(outcomes_distribution)
        outcomes_distribution.remove(extracted)
        image_registry = resources.get_images_by_precise_tag("Fato_" + str(extracted))
        for photo_name, photo in image_registry.items():
            images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"{normalize_username(player_name)} Ha pescato dal mazzo Fato " + str(
            modifier) + " cart" + ('a' if modifier == 1 else 'e') + ":")
    context.bot.send_media_group(
        chat_id=group_tid, media=images)
