import random

import telegram
from telegram import *
from telegram.ext import *
from campaignState import *
from library import *
from inlineKeyBoard import *

from mongoDB import *
import resources


# Tests Bot availability
def ping(update: Update, context: CallbackContext):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Pong")


# Starts enclave creation
# Sets the State to Enclave creation, players can no longer be added/removed
def create_enclave(update: Update, context: CallbackContext):
    player_id = update.message.from_user.id
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)

    if is_not_master_by_tid(player_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    # Check if there are enough players to start
    if get_num_of_players(campaign_id) < 2:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non sono presenti abbastanza giocatori per inizare una Campagna.")
        return

    if campaign_id is not None and get_state(campaign_id) != State.INIT.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    # enclave Creation

    if get_enclave_collection().find_one(
            {"campaign-id": campaign_id}
    ) is not None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="L'Enclave è già stato creato")
        return

    get_enclave_collection().insert_one(
        {"campaign-id": campaign_id, "poll-id": None, "enclave-type": None, "carenze": [], "vicinanze": [],
         "abitanti": [], "vantaggi": []}
    )

    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Creando enclave...")
    send_enclave_poll(update, context)

    update_state(campaign_id, State.CREATE_ENCLAVE)


# Sends the poll, poll is handled by the bot using a separate handler
# Poll is closed manually with /close_poll
def send_enclave_poll(update: Update, context: CallbackContext):
    # example poll structure: {'total_voter_count': 0, 'allows_multiple_answers': False, 'options': [{'text':
    # 'Ospedale', 'voter_count': 0}, {'text': 'Prigione', 'voter_count': 0}], 'type': 'regular',
    # 'explanation_entities': [], 'close_date': 1650550605, 'is_closed': False, 'is_anonymous': False, 'question':
    # "Votate per scegliere L'enclave:\n(in caso di pareggio, verrà estratto a caso)", 'open_period': 30,
    # 'id': '5983079779098689567'}
    reply = context.bot.sendPoll(
        chat_id=update.effective_chat.id,
        question="Votate per scegliere L'enclave:\n(in caso di pareggio, verrà estratto a caso)", is_anonymous=False,
        explanation="Per chiudere il poll e confermare le risposte, il Game Master deve chiamare il comando apposito.\n"
                    "Risposte successive alla chiusura non verranno prese in considerazione.",
        type='regular', allows_multiple_answers=False, options=["Ospedale", "Prigione"]
    )
    poll_id = reply["poll"]["id"]

    get_poll_collection().insert_one({
        "poll-id": poll_id, "options": ["ospedale", "prigione"], "voter-count": [0, 0], "poll-type": "enclave"
    })

    campaign_id = find_campaign_by_chat_id(update.message.chat_id)
    get_enclave_collection().find_one_and_update(
        {"campaign-id": campaign_id}, {"$set": {"poll-id": poll_id}}
    )


# Handles poll answers based on poll_type
def get_poll_answers(update: Update, context: CallbackContext):
    poll_type = get_poll_collection().find_one(
        {"poll-id": update.poll_answer.poll_id})["poll-type"]
    if poll_type == "enclave":
        index = update.poll_answer.option_ids[0]
        get_poll_collection().find_one_and_update(
            {"poll-id": update.poll_answer.poll_id}, {"$inc": {f"voter-count.{index}": 1}}
        )


# Closes the poll and handles results based on poll_type
def close_poll(update: Update, context: CallbackContext):
    campaign_id = find_campaign_by_chat_id(update.message.chat_id)
    poll_id = get_enclave_collection().find_one(
        {"campaign-id": campaign_id})["poll-id"]
    poll = get_poll_collection().find_one({"poll-id": poll_id})

    group = get_group_collection().find_one(
        {"telegram-id": update.message.chat_id}
    )
    current_campaign = group["current-campaign"]

    if is_not_master_by_tid(update.message.from_user.id, current_campaign):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if poll is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Il Poll è già stato chiuso o non è ancora stato creato.")
        return

    poll_type = poll["poll-type"]
    if poll_type == "enclave":
        ospedale = poll["voter-count"][0]
        prigione = poll["voter-count"][1]
        if ospedale > prigione:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Vince Ospedale!")
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$set": {"enclave-type": "Ospedale", "poll-id": -1}}
            )
            image_registry = resources.get_images_by_tag("Ospedale")
            images = []
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))
            context.bot.send_media_group(
                chat_id=update.effective_chat.id, media=images)

        elif prigione > ospedale:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Vince Prigione!")
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$set": {"enclave-type": "Prigione", "poll-id": -1}}
            )
            image_registry = resources.get_images_by_tag("Prigione")
            images = []
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))

            context.bot.send_media_group(
                chat_id=update.effective_chat.id, media=images)
        else:
            rand = random.choice(["Ospedale", "Prigione"])
            context.bot.send_message(
                chat_id=update.effective_chat.id, text=f"Vince {rand}!")
            get_enclave_collection().find_one_and_update(
                {"campaign-id": campaign_id}, {"$set": {"enclave-type": rand, "poll-id": -1}}
            )
            image_registry = resources.get_images_by_tag(rand)
            images = []
            for photo_name, photo in image_registry.items():
                images.append(telegram.InputMediaPhoto(open(photo, "rb")))

            context.bot.send_media_group(
                chat_id=update.effective_chat.id, media=images)

    # Deleting Poll...
    get_poll_collection().delete_one({"poll-id": poll_id})


# Start Create survivors state
def create_survivors(update: Update, context: CallbackContext):
    player_id = update.message.from_user.id
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)

    if is_not_master_by_tid(player_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if campaign_id is not None and get_state(campaign_id) != State.CREATE_ENCLAVE.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    # survivors Creation
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Sto creando i Sopravvissuti, Scrivetemi in privato per completare la creazione con il comando:"
             "\n/create_survivor")

    update_state(campaign_id, State.CREATE_SURVIVORS)


# Each player creates his own survivor, with automatic draw of the card
# Selection of Statistiche and Presente is handled with an inline keyboard
def create_survivor(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id
    player_id = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["_id"]

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if get_state(campaign_id) == State.INIT.value or get_state(campaign_id) == State.CREATE_ENCLAVE.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if not is_in_campaign(player_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Non sei più in questa campagna")
        return

    if get_survivor_collection().find_one(
            {"campaign-id": campaign_id, "player-id": player_id}
    ) is not None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Hai già creato il tuo superstite")
        return

    get_survivor_collection().insert_one(
        {"campaign-id": campaign_id, "player-id": player_id, "creation-done": False, "trauma": [], "passato": [],
         "presente": [], "ferocia": 0, "fermezza": 0, "anima": 0, "sopravvivenza": 0, "rapporto": [], "stress": 0}
    )
    draw_trauma(update, context)
    draw_passato(update, context)
    draw_presente(update, context)
    select_statistiche(update, context)


# Draws a Trauma card
# If the number of Traumas of the survivor reaches 4, the Players are notified of the imminent death of the survivor
def draw_trauma(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id
    player_id = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["_id"]
    player_name = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["name"]
    campaign_id = get_campaign_id_from_user_tid(player_tid)
    survivor = get_survivor_collection().find_one({"player-id": player_id})
    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="devi prima creare un Superstite")
        return

    if get_state(campaign_id) == State.INIT.value or get_state(campaign_id) == State.CREATE_ENCLAVE.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    traumi = survivor["trauma"]
    if traumi is not None:
        if len(traumi) > 2:
            group_chat_id = get_group_collection().find_one(
                {"current-campaign": campaign_id})["telegram-id"]
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="hai raggiunto un livello troppo alto di traumi e sei diventato un PNG.\n\n"
                     "Se desideri continuare a giocare puoi creare un nuovo superstite.")
            context.bot.send_message(
                chat_id=group_chat_id,
                text="Il superstite di " + player_name +
                     " ha raggiunto un livello troppo alto di traumi ed è diventato un PNG.\n\n"
                     "Il GM è pregato di creare un abitante che rappresenti il superstite appena convertito.\n\n"
                     "Se il player desidera continuare a giocare può creare un nuovo superstite.")
            get_survivor_collection().delete_one({"player-id": player_id})
            return

    entry_list = list(trauma.items())
    rand = random.choice(entry_list)
    while rand[0] in traumi:
        rand = random.choice(entry_list)

    get_survivor_collection().find_one_and_update(
        {"player-id": player_id}, {"$push": {"trauma": rand[0]}}
    )
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Hai pescato " + rand[0].split('-')[1] + " dal mazzo Trauma. Ora verrà aggiunto ai tuoi traumi.")

    image_registry = resources.get_images_by_precise_tag(rand[1])
    images = []
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=update.effective_chat.id, media=images)


# Draw a Passato card
def draw_passato(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id
    player_id = get_player_collection().find_one(
        {"telegram-id": player_tid}
    )["_id"]
    entry_list = list(passato.items())
    rand = random.choice(entry_list)

    get_survivor_collection().find_one_and_update(
        {"player-id": player_id}, {"$push": {"passato": rand[0]}}
    )
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Hai pescato " + rand[0].split('-')[1] + " dal mazzo Passato.")

    image_registry = resources.get_images_by_precise_tag(rand[1])
    images = []
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=update.effective_chat.id, media=images)


# Draw 2 Presente cards
# The player will be able to choose one between the two
def draw_presente(update: Update, context: CallbackContext):
    entry_list = list(presente.items())
    rand1 = random.choice(entry_list)
    entry_list.remove(rand1)
    rand2 = random.choice(entry_list)
    rands = [rand1, rand2]
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Hai pescato " + rands[0][0].split('-')[1] + " e " + rands[1][0].split('-')[1] + " dal mazzo Presente.")

    image_registry = resources.get_images_by_precise_tag(rands[0][1])
    images = []
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    image_registry = resources.get_images_by_precise_tag(rands[1][1])
    for photo_name, photo in image_registry.items():
        images.append(telegram.InputMediaPhoto(open(photo, "rb")))

    context.bot.send_media_group(
        chat_id=update.effective_chat.id, media=images)
    keyboard = [
        [
            InlineKeyboardButton(rands[0][0].split('-')[1], callback_data=rands[0][0]),
            InlineKeyboardButton(rands[1][0].split('-')[1], callback_data=rands[1][0]),
        ]
    ]
    update.message.reply_text(
        'Scegli un presente:',
        reply_markup=InlineKeyboardMarkup(keyboard))


# Used to call the inline Keyboard markup
def select_statistiche(update: Update, context: CallbackContext):
    keyboard_statistiche(update, context)


# Assigns Relationship between tho given Survivors
def assign_relationship(update: Update, context: CallbackContext):
    player_id = update.message.from_user.id
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non è ancora stata creata una campagna")
        return

    if is_not_master_by_tid(player_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if get_state(campaign_id) < State.CREATE_SURVIVORS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    players_in_campaign = get_players_in_campaign(campaign_id)

    for player in players_in_campaign:
        survivor = get_survivor_collection().find_one({"player-id": player})
        if survivor is None or survivor["creation-done"] == False:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Non tutti i giocatori hanno creato il loro personaggio.")
            return

    if len(context.args) != 2:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Per usare il comando devi usare la seguente sintassi /assign_rel @player1 @player2")
        return

    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    if len(campaign["creazione-rapporto"]) > 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Non puoi creare più rapporti contemporaneamente.\nFinisci prima di creare il rapporto precedente!")
        return

    player_names = get_player_names_by_campaign_id(campaign_id)
    name1 = context.args[0][1::]
    name2 = context.args[1][1::]

    if name1 not in player_names or name2 not in player_names:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Un player inserito non esiste o non è presente nella campagna")
        return

    if name1 == name2:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Un player non può avere relazioni con se stesso")
        return

    player1 = get_player_collection().find_one({"name": name1})
    player1_id = player1["_id"]
    player2 = get_player_collection().find_one({"name": name2})
    player2_id = player2["_id"]

    get_campaign_collection().find_one_and_update({"_id": campaign_id}, {
        "$push": {"creazione-rapporto": {"$each": [player1["_id"], player2["_id"]]}}})

    survivor = get_survivor_collection().find_one({"player-id": player1_id})
    relationships = survivor["rapporto"]
    entry_set = []
    for key in fato_rapporto.keys():
        entry_set.append(key)

    for element in relationships:
        if element["what"] in entry_set and element["other"] == player2_id:
            entry_set.remove(element["what"])

    keyboard_rapporto(update, context, entry_set)

    if get_state(campaign_id) != State.IN_GAME.value:
        update_state(campaign_id, State.CREATE_RELATIONSHIPS)


# Remove an already existing relationship between two survivors
# Only the name of one survivor is necessary
def remove_relationship(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    group_id = update.message.chat.id
    campaign_id = find_campaign_by_chat_id(group_id)

    if campaign_id is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non è ancora stata creata una campagna")
        return

    if is_not_master_by_tid(user_id, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if get_state(campaign_id) < State.CREATE_SURVIVORS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    players_in_campaign = get_players_in_campaign(campaign_id)

    if len(context.args) != 1:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Per usare il comando devi usare la seguente sintassi /remove_rel @player1")
        return

    name1 = context.args[0][1::]
    player_id = player_id_by_username(name1)

    if player_id is None or player_id not in players_in_campaign:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non esiste o non è presente nella campagna")
        return

    survivor = get_survivor_collection().find_one({"player-id": player_id})

    if survivor is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non possiede un survivor vivo al momento")
        return

    if len(survivor["rapporto"]) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"il player inserito non ha ancora alcun rapporto con altri player")
        return

    relationships = survivor["rapporto"]
    entry_set = []
    for key in fato_rapporto.keys():
        for element in relationships:
            if key == element["what"]:
                entry_set.append([key, element["other"]])

    keyboard_remove_rapporto(update, context, entry_set, player_id)


# Deletes an existing survivor (gets killed, can be called only by the player)
# If the Command is from the gm (with /kill), it will have the selected player ID and is_from_gm will be true
def delete_survivor(update: Update, context: CallbackContext, player_id=None, is_from_gm=False):
    player_tid = update.message.from_user.id
    if not is_from_gm:
        player_id = get_player_collection().find_one(
            {"telegram-id": player_tid}
        )["_id"]

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if get_state(campaign_id) == State.INIT.value or get_state(campaign_id) == State.CREATE_ENCLAVE.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if not is_in_campaign(player_id, campaign_id):
        if is_from_gm:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="Il player interessato non è più in questa campagna")
        else:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="Non sei più in questa campagna")
        return

    if get_survivor_collection().find_one(
            {"campaign-id": campaign_id, "player-id": player_id}
    ) is None:
        if is_from_gm:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="Il player interessato non ha ancora creato un superstite")
        else:
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="Non hai ancora creato un superstite")
        return

    get_survivor_collection().find_one_and_delete(
        {"campaign-id": campaign_id, "player-id": player_id}
    )
    campaign = get_campaign_collection().find_one({"_id": campaign_id})

    for pngID in campaign["png"]:
        get_png_collection().find_one_and_update({"_id": pngID}, {"$pull": {"allies": player_id}})
    for other_player_id in campaign["players"]:
        get_survivor_collection().find_one_and_update({"player-id": other_player_id},
                                                      {"$pull": {"rapporto": {"other": player_id}}})

    if not is_from_gm:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Hai eliminato il superstite con successo")

    group_chat_id = get_group_collection().find_one(
        {"current-campaign": campaign_id})["telegram-id"]

    context.bot.send_message(
        chat_id=group_chat_id,
        text=f"{player_username_by_id(player_id)} è morto!")


# Kills an existing survivor (gets killed, can be called only by the GM)
def kill_survivor(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id
    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if is_not_master_by_tid(player_tid, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if len(context.args) != 1:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Formato sbagliato. Il comando dev'essere /kill @player da uccidere")
        return

    user = context.args[0]

    if user[0] != "@":
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=user + " non è un utente valido")
        return

    user = user.removeprefix("@")

    if is_master_by_name(user, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Il Game Master non può possedere un survivor")
        return

    delete_survivor(update, context, player_id_by_username(user), True)


# Adds a PNG to the campaign
def add_png(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if is_not_master_by_tid(player_tid, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if get_state(campaign_id) < State.CREATE_RELATIONSHIPS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    entry_list = list(personaggi.items())
    remaining_pngs = []
    pngs_in_campaing = get_png_in_campaign(campaign_id)

    for png in entry_list:
        if not png_in_campaign(png[0], pngs_in_campaing):
            remaining_pngs.append(png)

    if len(remaining_pngs) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Hai raggiunto il massimo numero di PNG")
        return

    keyboard_add_png(update, context, remaining_pngs)

    if get_state(campaign_id) != State.IN_GAME.value:
        update_state(campaign_id, State.CREATE_PNGS)


# Kills an existing PNG, it will be possible to be added again later
def kill_png(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if is_not_master_by_tid(player_tid, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if get_state(campaign_id) < State.CREATE_RELATIONSHIPS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    entry_list = list(personaggi.items())
    remaining_pngs = []
    pngs_in_campaing = get_png_in_campaign(campaign_id)

    for png in entry_list:
        if png_in_campaign(png[0], pngs_in_campaing):
            remaining_pngs.append(png)

    if len(remaining_pngs) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Non ci sono PNG da uccidere")
        return

    keyboard_remove_png(update, context, remaining_pngs)


# Sets the campaign into the game states, no more structural changes will be possible
def start_game(update: Update, context: CallbackContext):
    player_tid = update.message.from_user.id

    campaign_id = get_campaign_id_from_user_tid(player_tid)
    if get_campaign_collection().find_one(
            {"_id": campaign_id}
    ) is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    if is_not_master_by_tid(player_tid, campaign_id):
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare quel comando.\nNon sei il Game Master")
        return

    if get_state(campaign_id) < State.CREATE_PNGS.value:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Non puoi chiamare questo comando adesso.")
        return

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="La partita è ora cominciata\nDa ora non saranno più possibili modifiche strutturali della campagna")

    update_state(campaign_id, State.IN_GAME)
