# Hera are all the used dictionaries

# id: 00
carenze = {
    "00-cibo": "cibo",
    "00-medicinali": "medicinali",
    "00-privacy": "privacy",
    "00-sicurezza": "sicurezza",
    "00-armi": "armi",
    "00-comodità": "comodità"}

# id: 01
vicinanze = {
    "01-avamposto": "un pericoloso avamposto militare",
    "01-foresta": "una foresta rigogliosa",
    "01-enclave": "un'enclave abbandonata e non sicura",
    "01-montagne": "un'insuperabile catena montuosa",
    "01-acqua": "un vasto specchio d'acqua",
    "01-città": "una grande città brulicante di zombie",
    "01-uffici": "un complesso di uffici barricato",
    "01-porto": "una darsena o porto",
    "01-polizia": "un posto di polizia fortificato",
    "01-centrale": "una centrale elettrica",
    "01-quartiere": "un quartiere cittadino in rovina",
    "01-supermercato": "un grande centro commerciale"
}

# id: 02
abitanti = {
    "02-detenuto": "un detenuto famoso",
    "02-profughi": "una dozzina di profughi dalla città",
    "02-contadini": "la famiglia di una fattoria locale",
    "02-soldati": "una coppia di soldati",
    "02-famiglia": "una famiglia della periferia",
    "02-secondini": "un gruppetto di secondini",
    "02-direttore": "il direttore della prigione",
    "02-pazienti": "un gruppo di pazienti superstiti",
    "02-funzionario": "un funzionario cittadino",
    "02-medico": "un medico esperto",
    "02-amministratore": "un amministratore ospedaliero",
    "02-ufficiale": "un ufficiale dell'esercito",
    "02-poliziotti": "un gruppetto di poliziotti"
}

# id: 03
vantaggi = {
    "03-elicottero": "Vantaggi_1",
    "03-generatori": "Vantaggi_2",
    "03-quarantena": "Vantaggi_3",
    "03-mensa": "Vantaggi_4",
    "03-rimessa": "Vantaggi_5",
    "03-infermeria": "Vantaggi_6",
    "03-armeria": "Vantaggi_7",
    "03-recinzioni solide": "Vantaggi_8"
}

# id: 04
trauma = {
    "04-xenofobo": "Trauma_1",
    "04-predatorio": "Trauma_2",
    "04-fissato": "Trauma_3",
    "04-compulsivo": "Trauma_4",
    "04-ossessivo": "Trauma_5",
    "04-autoritario": "Trauma_6",
    "04-testardo": "Trauma_7",
    "04-invadente": "Trauma_8",
    "04-insensibile": "Trauma_9",
    "04-vigliacco": "Trauma_10",
    "04-freddo": "Trauma_11",
    "04-arrogante": "Trauma_12",
    "04-tossico": "Trauma_13",
    "04-esplosivo": "Trauma_14",
    "04-avventato": "Trauma_15",
    "04-crudele": "Trauma_16"
}

# id: 05
passato = {
    "05-musicista": "Passato_1",
    "05-teppista": "Passato_2",
    "05-veterano": "Passato_3",
    "05-allenatore": "Passato_4",
    "05-soccorritore": "Passato_5",
    "05-attore": "Passato_6",
    "05-lottatore": "Passato_7",
    "05-motociclista": "Passato_8",
    "05-survivalista": "Passato_9",
    "05-adolescente": "Passato_10",
    "05-poliziotto": "Passato_11",
    "05-ex detenuto": "Passato_12",
    "05-dottore": "Passato_13",
    "05-psichiatra": "Passato_14",
    "05-prete": "Passato_15",
    "05-secondino": "Passato_16"
}

# id: 06
presente = {
    "06-vendetta": "Presente_1",
    "06-tiranno": "Presente_2",
    "06-diplomatico": "Presente_3",
    "06-scienziato": "Presente_4",
    "06-profeta": "Presente_5",
    "06-archivista": "Presente_6",
    "06-bastian contrario": "Presente_7",
    "06-visionario": "Presente_8",
    "06-mediatore": "Presente_9",
    "06-guardiano": "Presente_10",
    "06-seguace": "Presente_11",
    "06-macellaio": "Presente_12",
    "06-sicario": "Presente_13",
    "06-sopravissuto": "Presente_14",
    "06-cuoco": "Presente_15",
    "06-esploratore": "Presente_16"
}

# id: 07
statistiche = {
    "07-uno": "1",
    "07-due": "2",
    "07-tre": "3",
}

# id: 08
fato_rapporto = {
    "08-vi incolpate": "Fato_1_R",
    "08-un abitante se ne deve andare": "Fato_2_R",
    "08-avevate una relazione": "Fato_3_R",
    "08-avete una relazione": "Fato_4_R",
    "08-avete compiuto un atto terribile": "Fato_5_R",
    "08-avete a cuore un altro abitante": "Fato_6_R",
    "08-pianificate miglioramenti": "Fato_7_R",
    "08-scontro breve ma intenso": "Fato_8_R"
}

# id: 09
personaggi = {
    "09-Leo Clarke": "Abitanti_1",
    "09-Clayton Mathis": "Abitanti_2",
    "09-Danny Delgado": "Abitanti_3",
    "09-Darrian Spiros": "Abitanti_4",
    "09-Alayna Vass": "Abitanti_5",
    "09-Virginia Kempf": "Abitanti_6",
    "09-Hazel Nemeth": "Abitanti_7",
    "09-Juanita Lopez": "Abitanti_8",
    "09-Ryan Hyeong": "Abitanti_9",
    "09-Demarcus Nation": "Abitanti_10",
    "09-Maya Viswan": "Abitanti_11",
    "09-Sally Nelson": "Abitanti_12",
    "09-Opal Ramirez": "Abitanti_13",
    "09-Percy Ward": "Abitanti_14",
    "09-Travis Conrad": "Abitanti_15",
    "09-Roxanna Brower": "Abitanti_16"
}
