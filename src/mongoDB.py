import pymongo
import os
from pathlib import Path
from dotenv import load_dotenv

# Variables used by DB interactions
dotenv_path = Path('bot.env')
load_dotenv(dotenv_path=dotenv_path)

mongo_client = pymongo.MongoClient(os.getenv("ZW_MONGO_URI"))
zombie_world_db = mongo_client[os.getenv("ZW_DATABASE")]


# Returns Campaign collection
def get_campaign_collection():
    return zombie_world_db["campaign"]


# Returns Player collection
def get_player_collection():
    return zombie_world_db["player"]


# Returns Group collection
def get_group_collection():
    return zombie_world_db["group"]


# Returns Enclave collection
def get_enclave_collection():
    return zombie_world_db["enclave"]


# Returns Poll collection
def get_poll_collection():
    return zombie_world_db["poll"]


# Returns Survivor collection
def get_survivor_collection():
    return zombie_world_db["survivor"]


# Returns PNG collection
def get_png_collection():
    return zombie_world_db["png"]


# Returns Campaign collection
def get_zombie_deck_collection():
    return zombie_world_db["zombie-deck"]


# Returns Players participating in the given Campaign
def get_players_in_campaign(campaign_id):
    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    return campaign["players"]


# Returns the current Campaign played in the given Group
def find_campaign_by_chat_id(group_tid):
    group = get_group_collection().find_one({"telegram-id": group_tid})
    if group is None:
        raise Exception("Group is None")
    return group["current-campaign"]


# Returns the number of players inside a given Campaign
def get_num_of_players(campaign_id):
    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    if campaign is None:
        raise Exception("Campaign is None")

    return len(campaign["players"])


# Check if the given user is NOT the master of a given campaign by using TID
def is_not_master_by_tid(user_tid, campaign):
    player = get_player_collection().find_one(
        {"telegram-id": user_tid}
    )

    master = get_campaign_collection().find_one(
        {"_id": campaign}
    )

    if (master is not None) and (player is not None) and (master["game-master"] == player["_id"]):
        return False
    else:
        return True


# Check if the given user IS the master of a given campaign by using his username
def is_master_by_name(user, campaign):
    player = get_player_collection().find_one(
        {"name": user}
    )
    master = get_campaign_collection().find_one(
        {"_id": campaign}
    )

    if (master is not None) and (player is not None) and (master["game-master"] == player["_id"]):
        return True
    else:
        return False


# Return the campaign in which the given user is playing by using TID
def get_campaign_id_from_user_tid(user_tid):
    campaign_id = get_player_collection().find_one(
        {"telegram-id": user_tid}
    )["campaign-id"]
    if campaign_id is not None:
        return campaign_id
    else:
        return None


# Returns true if the user is in the given campaign
def is_in_campaign(user, campaign):
    campaign_players = get_campaign_collection().find_one(
        {"_id": campaign}
    )["players"]
    if campaign_players is not None and user not in campaign_players:
        return False
    return True


# Returns all the usernames of players in a given campaign
def get_player_names_by_campaign_id(campaign_id):
    player_names = []
    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    player_ids = campaign["players"]
    for playerID in player_ids:
        player = get_player_collection().find_one({"_id": playerID})
        player_names.append(player["name"])
    return player_names


# Returns all the PNGs in a given campaign
def get_png_in_campaign(campaign_id):
    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    pngs = campaign["png"]
    return pngs


# Returns TRUE if the PNG is already in the campaign
def png_in_campaign(png_name, png_list):
    if png_list is None:
        return False

    for png in png_list:
        if png_name == get_png_collection().find_one({"_id": png})["name"]:
            return True
    return False


# Returns the player ID by their telegram username
def player_id_by_username(username):
    player = get_player_collection().find_one({"name": username})
    if player is not None:
        return player["_id"]
    else:
        return None


# Returns the player username by their id
def player_username_by_id(player_id):
    player = get_player_collection().find_one({"_id": player_id})
    if player is not None:
        return player["name"]
    else:
        return None


# Returns the player ID by their TID
def player_id_by_tid(player_tid):
    player = get_player_collection().find_one({"telegram-id": player_tid})
    if player is not None:
        return player["_id"]
    else:
        return None


# Returns the player TID by their ID
def player_tid_by_id(player_id):
    player = get_player_collection().find_one({"_id": player_id})
    if player is not None:
        return player["telegram-id"]
    else:
        return None


# Returns the group TID by campaign id
def group_tid_by_campaign_id(campaign_id):
    group = get_group_collection().find_one({"current-campaign": campaign_id})
    if group is not None:
        return group["telegram-id"]
    else:
        return None


# Returns the group TID by campaign id
def gm_id_by_campaign_id(campaign_id):
    campaign = get_campaign_collection().find_one({"_id": campaign_id})
    if campaign is not None:
        return campaign["game-master"]
    else:
        return None


# Returns true if there is already a zombie deck for the campaign
def is_zombie_deck_present(campaign_id):
    deck = get_zombie_deck_collection().find_one({"campaign-id": campaign_id})
    if deck is not None:
        return True
    else:
        return False


# Returns the max number of possible draws
def zombie_deck_max_draws(campaign_id):
    deck = get_zombie_deck_collection().find_one({"campaign-id": campaign_id})
    if deck is not None:
        return len(deck["deck"])
    else:
        return 0


# Resets the deck to its original cards
def reset_zombie_deck(campaign_id):
    outcomes_distribution = [1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 5, 5, 5, 5, 5]
    get_zombie_deck_collection().find_one_and_update({"campaign-id": campaign_id},
                                                     {"$set": {"deck": outcomes_distribution}})


# Sets the deck to the current remaining cards
def set_zombie_deck(campaign_id, deck):
    get_zombie_deck_collection().find_one_and_update({"campaign-id": campaign_id},
                                                     {"$set": {"deck": deck}})


# Retrieve PNGs in campaign
def get_pngs_in_campaign(campaign_id):
    pngs = get_png_collection().find({"campaign-id": campaign_id})

    return [png["name"].split("-")[1] for png in pngs]
