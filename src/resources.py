import os
import string
from pathlib import Path

from flask import g

# Will contain all the resources once the server starts
registry = {}


# Load images once on the server, used for faster uploading
def load_images():
    print("Loading resources...")
    path = "resources/images/"
    files = [name for name in os.listdir(path)]

    for file in files:
        split_file_name_type = file.split(".")
        file_name = split_file_name_type[0]
        registry[file_name] = Path(path + file)
    print("Loading resources completed!")


# Get all the images with the name that starts with the given string
def get_images_by_tag(tag: string):
    return {key: val for key, val in registry.items() if key.startswith(tag)}


# Get all the images with the same name as the given string
def get_images_by_precise_tag(tag: string):
    return {key: val for key, val in registry.items() if key == tag}
