
# Removes the underscores that generate conflict with markup
def normalize_username(username):
    return username.replace("_", " ")

# Tries to convert a string to an int and returns TRUE if operation succeded, otherwise false
def is_a_number(string):
    try:
        int(string)
        return True
    except ValueError:
        return False
