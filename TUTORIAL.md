# Zombie World Bot Tutorial

Per utilizzare il bot usare il seguente telegram link: https://t.me/zombie_world_bot

## Comandi

Le funzioni possono essere di tipo ***Private*** se possono essere chiamate in chat privata con il bot, ***Group***
se chiamate in un gruppo con il bot attivo, ***All*** se possono essere chiamate ovunque.

Le funzioni con tipo ***GM*** possono essere chiamate solo dal creatore della campagna, mentre quelle di tipo
***Survivor*** solo dai giocatori attivi, ovvero che presentano un survivor vivo.

| **Number** | **Command**                                                             |    **Filter**     |
|:-----------|:------------------------------------------------------------------------|:-----------------:|
| 1          | [/start](TUTORIAL.md#1-start)                                           |        All        |
| 2          | [/ping](TUTORIAL.md#2-ping)                                             |        All        |
| 3          | [/create](TUTORIAL.md#3-create)                                         |       Group       |
| 4          | [/delete](TUTORIAL.md#4-delete)                                         |     Group, GM     |
| 5          | [/add @player1 @player2 ...](TUTORIAL.md#5-add)                         |     Group, GM     |
| 6          | [/remove @player1 @player2 ...](TUTORIAL.md#6-remove)                   |     Group, GM     |
| 7          | [/create_enclave](TUTORIAL.md#7-create_enclave)                         |     Group, GM     |
| 8          | [/close_poll](TUTORIAL.md#8-close_poll)                                 |     Group, GM     |
| 9          | [/carenze](TUTORIAL.md#9-carenze)                                       |       Group       |
| 10         | [/vicinanze](TUTORIAL.md#10-vicinanze)                                  |       Group       |
| 11         | [/abitanti](TUTORIAL.md#11-abitanti)                                    |       Group       |
| 12         | [/vantaggi](TUTORIAL.md#12-vantaggi)                                    |       Group       |
| 13         | [/create_survivors](TUTORIAL.md#13-create_survivors)                    |     Group, GM     |
| 14         | [/create_survivor](TUTORIAL.md#14-create_survivor)                      | Private, Survivor |
| 15         | [/delete_survivor](TUTORIAL.md#15-delete_survivor)                      | Private, Survivor |
| 16         | [/assign_rel @player1 @player2](TUTORIAL.md#16-assign_rel)              |     Group, GM     |
| 17         | [/remove_rel @player](TUTORIAL.md#17-remove_rel)                        |     Group, GM     |
| 18         | [/add_png](TUTORIAL.md#18-add_png)                                      |    Private, GM    |
| 19         | [/kill_png](TUTORIAL.md#19-kill_png)                                    |    Private, GM    |
| 20         | [/start_game](TUTORIAL.md#20-start_game)                                |     Group, GM     |
| 21         | [/draw_trauma](TUTORIAL.md#21-draw_trauma)                              | Private, Survivor |
| 22         | [/draw_survivor](TUTORIAL.md#22-draw_survivor)                          |  Group, Survivor  |
| 23         | [/draw_zombie](TUTORIAL.md#23-draw_zombie)                              |  Group, Survivor  |
| 24         | [/draw_fate](TUTORIAL.md#24-draw_fate)                                  |  Group, Survivor  |
| 25         | [/increase_stress](TUTORIAL.md#25-increase_stress)                      |  Group, Survivor  |
| 26         | [/decrease_stress](TUTORIAL.md#26-decrease_stress)                      |  Group, Survivor  |
| 27         | [/increase_png_stress \*png_name\*](TUTORIAL.md#27-increase_png_stress) |     Group, GM     |
| 28         | [/show_rel](TUTORIAL.md#28-show_rel)                                    |       Group       |     
| 29         | [/show_pngs](TUTORIAL.md#29-show_pngs)                                  |       Group       |
| 30         | [/show_png \*png_name\*](TUTORIAL.md#30-show_png)                       |  Group, Survivor  |
| 31         | [/show_enclave](TUTORIAL.md#31-show_enclave)                            |       Group       |
| 32         | [/show_survivor](TUTORIAL.md#32-show_survivor)                          |  Group, Survivor  |
| 33         | [/show_trauma](TUTORIAL.md#33-show_trauma)                              |  Group, Survivor  |
| 34         | [/show_past](TUTORIAL.md#34-show_past)                                  |  Group, Survivor  |
| 35         | [/add_ally \*png_name\* ](TUTORIAL.md#35-add_ally)                      |  Group, Survivor  |
| 36         | [/kill @player](TUTORIAL.md#36-kill)                                    |     Group, GM     |
| 37         | [/base_moves](TUTORIAL.md#37-base_moves)                                |       Group       |
| 38         | [/zombie_moves](TUTORIAL.md#38-zombie_moves)                            |  Group, Survivor  |
| 39         | [/gm_moves](TUTORIAL.md#39-gm_moves)                                    |    Private, GM    |
| 40         | [/enclave_helper](TUTORIAL.md#40-enclave_helper)                        |      Private      |
| 41         | [/survivor_helper](TUTORIAL.md#41-survivor_helper)                      |      Private      |
| 42         | [/help](TUTORIAL.md#42-help)                                            |        All        |


---

## Come creare una campagna

<ol type="I">
  <li>Ogni giocatore che desidera partecipare a una campagna <b>deve</b> scrivere il comando <code>/start</code>, 
altrimenti risulta impossibile per il bot mandare messaggi o risposte durante la partita (ciò vale anche quando
un utente ha disattivato il bot e desidera riattivarlo o quando non riceve più messaggi dal bot poichè è passato troppo
tempo).</li>
  <br>
  <li>Il <b>GM</b> ora deve scrivere il comando <code>/create</code> nel gruppo dove vuole procedere con la campagna;
ciò darà inizio alla sequenza di comandi per creare la campagna, l'enclave e le schede dei sopravvisuti. Sarà 
possibile chiamare il comando <code>/delete</code> in qualsiasi momento per eliminare <b>DEFINITIVAMENTE</b> la
campagna.</li>
<br>
  <li>Il <b>GM</b> può ora aggiungere tutti i player desiderati con il comando <code>/add</code>, o rimuoverli
con <code>/remove</code>. Quando sono stati aggiunti tutti i partecipanti, il <b>GM</b> potrà iniziare la creazione 
dell'enclave. <i><b>Una volta iniziata la creazione dell'enclave, non sarà più possibile rimuovere o aggiungere altri 
player.</b></i></li>
<br>
 <li>Il <b>GM</b> deve utilizzare il comando <code>/create_enclave</code> per dare inizio alla creazione 
dell'enclave. Esso genererà una <b>poll</b>, nella quale tutti possono votare quale tipo di enclave scegliere. La
<b>poll</b> viene chiusa nel momento in cui il <b>GM</b> chiama il comando <code>/close_poll</code>, che 
decreta il vincitore <i>(in caso di pareggio è estratto a caso).</i></li>
<br>
 <li>Il <b>GM</b> può ora completare la creazione dell'enclave chiamando i vari comandi che assegnano gli attributi:
<code>/carenze</code>, <code>/vicinanze</code>, <code>/abitanti</code>, <code>/vantaggi</code> <i>(l'ordine
non è importante)</i>. </li>
<br>
 <li>Una volta finito di assegnare gli attributi, il <b>GM</b> chiama il comando <code>/create_survivors</code>, 
il quele permette ai vari giocatori di creare il proprio superstite in <b><i>PRIVATO</i></b> utilizzando il 
comando <code>/create_survivor</code>. Ogni volta che un survivor viene completato, il gruppo è notificato.
In caso un player commettesse un errore nella creazione, è sempre possibile utilizzare il comando
<code>/delete_survivor</code> che cancella <b>DEFINITIVAMENTE</b> il superstite e permette di crearne uno nuovo.</li>
<br>
 <li>Finita la creazione dei survivors, il <b>GM</b> inizia ad assegnare le relazioni tra i vari sopravvissuti,
 scegliendole tra quelle disponibili. Per assegnare una relazione deve utilizzare il comando <code>/assign_rel</code>.
Per rimuovere una relazione assegnata erroneamente è possibile utilizzare il comando <code>/remove_rel</code>.</li>
<br>
 <li>Dopo aver assegnato tutte le relazioni, il <b>GM</b> può iniziare a creare i <b><i>PNG</i></b>. Per creare un
<i>PNG</i> basta utilizzare il comando <code>/add_png</code>. Nel caso sia stato creato erroneamente un <i>PNG</i> 
sbagliato, è possibile ucciderlo con il comando <code>/kill_png</code>.</li>
<br>
 <li>Infine il <b>GM</b> da inizio al gioco con il comando <code>/start_game</code>. Dopo averlo usato, non sarà più
possibile apportare modifiche strutturali alla campagna (come aggiungere o rimuovere giocatori). </li>
<br>
 <li>Diventa ora possibile utilizzare tutti i comandi di gioco, come i vari <code>/show...</code> oppure gli 
<code>/draw...</code>. Nel caso sorgessero dubbi su come utilizzare un comando, consultare la tabella sovrastante.</li>
</ol>

## Descrizione dei comandi

### 1. /start

Il comando `/start` è molto semplice: una volta chiamato nella chat privata verrai registrato tra gli utenti, il GM
potrà aggiungerti nel momento in cui farà /add *@il_tuo_username* e il bot verrà attivato (quindi potrà mandarti dei
messaggi relativi al gioco).

![start](resources/tutorial-screenshots/start.png)

### 2. /ping

Il comando `/ping` verifica solamente se il bot è attivo inviando come risposta *pong*.

![ping](resources/tutorial-screenshots/ping.png)

### 3. /create

Il comando `/create` crea la campagna. L'utente che ha utilizzato il comando diventa automaticamente il **Game Master**.
Una volta creata la campagna è possibile iniziare ad aggiungere o rimuovere i giocatori. La campagna può essere
cancellata in qualsiasi momento con `/delete`. Viene inoltre inviato il codice di riconoscimento della campagna, utile
nel caso sia necessario supporto.

![create](resources/tutorial-screenshots/create.png)

### 4. /delete

Il comando `/delete` elimina la campagna e tutti i dati relativi ad essa (compresi *survivors* e
*enclave*).

![delete](resources/tutorial-screenshots/delete.png)

### 5. /add

Il comando `/add` aggiunge uno o più giocatori alla campagna. Per aggiungere più giocatori, basta passare come parametri
più username (`/add @player1 @player2 @player3`), mentre se è necessario aggiungere un solo giocatore è sufficiente
passare un solo parametro `/add @player1`. Altre sintassi come l'utilizzo di `/add` senza parametri o il passaggio di
player non esistenti otterranno come risposte i relativi messaggi di errore.

![add-1](resources/tutorial-screenshots/add_1.png)
![add-2](resources/tutorial-screenshots/add_2.png)

![add-3](resources/tutorial-screenshots/add_3.png)
![add-4](resources/tutorial-screenshots/add_4.png)

### 6. /remove

Il comando `/remove` rimuove uno o più giocatori alla campagna. Per rimuovere più giocatori, basta passare come
parametri più username (`/remove @player1 @player2 @player3`), mentre se è necessario rimuovere un solo giocatore è
sufficiente passare un solo parametro `/remove @player1`. Altre sintassi come l'utilizzo di `/remove` senza parametri o
il passaggio di player non esistenti otterranno come risposte i relativi messaggi di errore. Se dopo uno `/remove`
il numero di giocatori scende sotto il 2, il GM viene avvisato che sono troppo pochi per apprezzare una partita.

![remove-1](resources/tutorial-screenshots/remove_2.png)
![remove-2](resources/tutorial-screenshots/remove_3.png)

![remove-3](resources/tutorial-screenshots/remove_4.png)
![remove-4](resources/tutorial-screenshots/remove_5.png)

![remove-5](resources/tutorial-screenshots/remove_1.png)

### 7. /create_enclave

Il comando `/create_enclave` da inizio alla creazione dell'enclave mostrando una *poll* che permette di votare quale
tipo di enclave generare. Il poll potrà essere chiuso tramite l'apposito comando `/close_poll`. Una volta chiamato
`/create_enclave`, sarà possibile concludere la sua creazione aggiungendo i vari attributi con i comandi appositi:

- `/carenze`
- `/vicinanze`
- `/abitanti`
- `/vantaggi`

*(Non è necessario chiamare questi comandi nell'ordine riportato sopra)*

In caso non siano presenti abbastanza giocatori, verrà mostrato il rispettivo messaggio di errore.

![create-enclave-1](resources/tutorial-screenshots/create_enclave_1.png)

![create-enclave-2](resources/tutorial-screenshots/create_enclave_2.png)

### 8. /close_poll

Il comando `/close_poll` chiude la poll per scegliere il tipo di enclave e ne mostra le carte. In caso di contesa, il
vincitore verrà estratto casualmente tra i due.

![create-enclave-3](resources/tutorial-screenshots/create_enclave_3.png)

### 9. /carenze

Il comando `/carenze` permette di aggiungere una carenza tramite *inline keyboard* se non era già presente nell'enclave,
altrimenti essa viene rimossa.

![carenze-1](resources/tutorial-screenshots/carenze_1.png)

![carenze-2](resources/tutorial-screenshots/carenze_2.png)
![carenze-3](resources/tutorial-screenshots/carenze_3.png)

### 10. /vicinanze

Il comando `/vicinanze` permette di aggiungere una vicinanza tramite *inline keyboard* se non era già presente nell'enclave,
altrimenti essa viene rimossa.

![vicinanze-1](resources/tutorial-screenshots/vicinanze_1.png)

![vicinanze-2](resources/tutorial-screenshots/vicinanze_2.png)
![vicinanze-3](resources/tutorial-screenshots/vicinanze_3.png)

### 11. /abitanti

Il comando `/abitanti` permette di aggiungere un abitante tramite *inline keyboard* se non era già presente nell'enclave,
altrimenti esso viene rimosso.

![abitanti-1](resources/tutorial-screenshots/abitanti_1.png)

![abitanti-2](resources/tutorial-screenshots/abitanti_2.png)
![abitanti-3](resources/tutorial-screenshots/abitanti_3.png)

### 12. /vantaggi

Il comando `/vantaggi` permette di aggiungere un vantaggio tramite *inline keyboard* se non era già presente nell'enclave,
altrimenti esso viene rimosso.

![vantaggi-1](resources/tutorial-screenshots/vantaggi_1.png)

![vantaggi-2](resources/tutorial-screenshots/vantaggi_2.png)
![vantaggi-3](resources/tutorial-screenshots/vantaggi_3.png)

### 13. /create_survivors

Il comando `/create_survivors` da inizio alla creazione dei sopravvisuti e permette ai vari giocatori di creare il 
proprio superstite in ***PRIVATO*** utilizzando il comando `/create_survivor`.

![create_survivors-1](resources/tutorial-screenshots/create_survivors.png)

### 14. /create_survivor

Il comando `/create_survivor` deve essere utilizzato in privato da ogni player della campagna per creare il proprio
superstite. Nella creazione, il bot estrae in automatico le carte passato e trauma, le quali rimangono ***nascoste***, 
mentre invece permette all'utente di scegliere una delle due carte presente estratte. Risulta anche necessario 
l'assegnamento delle statistiche del superstite, che viene effettuato attribuendo secondo l'ordine stabilito una 
statistica per volta utilizzando uno dei tre valori possibili. Non è possibile creare più di un superstite 
contemporaneamente e, una volta creatone uno, verrà inviato un messaggio di notifica al gruppo. É possibile eliminare
in qualsiasi momento il proprio superstite in caso sia stato commesso un errore con il comando `/delete_survivor`.

![create_survivor-1](resources/tutorial-screenshots/create_survivor_2.png)
![create_survivor-2](resources/tutorial-screenshots/create_survivor_3.png)
![create_survivor-3](resources/tutorial-screenshots/create_survivor_4.png)

![create_survivor-4](resources/tutorial-screenshots/create_survivor_5.png)
![create_survivor-5](resources/tutorial-screenshots/create_survivor_1.png)

### 15. /delete_survivor

Il comando `/delete_survivor` può essere utilizzato in qualsiasi momento in caso sia stato commesso un errore
al fine di eliminare **DEFINITIVAMENTE** il superstite. Non è possibile utilizzare il comando nel caso in cui il 
player non possieda alcun superstite vivo.

![delete_survivor-1](resources/tutorial-screenshots/delete_survivor_2.png)
![delete_survivor-2](resources/tutorial-screenshots/delete_survivor_1.png)

![delete_survivor-3](resources/tutorial-screenshots/delete_survivor_3.png)

### 16. /assign_rel

Il comando `/assign_rel @player1 @player2` viene utilizzato dal **GM** per assegnare le relazioni tra due superstiti.
 Vanno inseriti esattamente due username validi come parametri dopo il comando, altrimenti viene generato un messaggio 
di errore. La scelta della Relazione è effettuata tra quelle disponibili tra i due survivor *(non ci può essere la 
stessa relazione tra due superstiti più di una volta)*. In caso di assegnamento erroneo, è sempre possibile rimuovere la
relazione con il comando `/remove_rel @player1` oppure `/remove_rel @player2`.

![assign_relationship-1](resources/tutorial-screenshots/assign_rel_1.png)
![assign_relationship-2](resources/tutorial-screenshots/assign_rel_2.png)

![assign_relationship-3](resources/tutorial-screenshots/assign_rel_3.png)
![assign_relationship-4](resources/tutorial-screenshots/assign_rel_4.png)

### 17. /remove_rel

Il comando `/remove_rel @player` viene utilizzato dal **GM** per rimuovere una relazione tra due superstiti. É possibile
passare uno qualsiasi tra i due giocatori come parametro, poiché verrà presentata una *inline keyboard* con la quale scegliere
la relazione, la quale è contraddistinta dal tipo e dal superstite con il quale essa è condivisa. Risulta necessario
passare un player valido, altrimenti viene generato un messaggio di errore. Anche nel caso in cui il player inserito
 non possiede alcun rapporto con altri superstiti viene generato un errore.

![remove_relationship-1](resources/tutorial-screenshots/remove_rel_1.png)
![remove_relationship-2](resources/tutorial-screenshots/remove_rel_2.png)

![remove_relationship-3](resources/tutorial-screenshots/remove_rel_3.png)
![remove_relationship-4](resources/tutorial-screenshots/remove_rel_4.png)

![remove_relationship-5](resources/tutorial-screenshots/remove_rel_5.png)

### 18. /add_png

Il comando `/add_png` viene utilizzato dal **GM** per aggiungere dei *Player Non Giocanti*. É possibile scegliere tra
i **PNG** che non sono già stati aggiunti all'enclave. Il comando è utilizzabile ***solo in chat privata.***
Una volta aggiunto, il gruppo viene notificato.

![add_png-1](resources/tutorial-screenshots/add_png_1.png)
![add_png-2](resources/tutorial-screenshots/add_png_2.png)

![add_png-3](resources/tutorial-screenshots/add_png_3.png)

### 19. /kill_png

Il comando `/kill_png` viene utilizzato dal **GM** per rimuovere dei *Player Non Giocanti*. É possibile scegliere tra
i **PNG** che sono già stati aggiunti all'enclave. Il comando è utilizzabile ***solo in chat privata.***
Una volta rimosso, il gruppo viene notificato. Se non sono presenti *PNG*, viene generato un messaggio di errore.

![kill_png-1](resources/tutorial-screenshots/kill_png_1.png)
![kill_png-2](resources/tutorial-screenshots/kill_png_2.png)
![kill_png-3](resources/tutorial-screenshots/kill_png_3.png)

![kill_png-4](resources/tutorial-screenshots/kill_png_4.png)

### 20. /start_game

Il comando `/start_game` viene utilizzato dal **GM** per dare inizio alla partita. Dopo averlo usato, non sarà più
possibile apportare modifiche strutturali alla campagna *(come aggiungere o rimuovere giocatori)*.

![start-game](resources/tutorial-screenshots/start_game.png)

### 21. /draw_trauma

Il comando `/draw_trauma` viene utilizzato dai **Players** per pescare una carta dal mazzo trauma 
***in privato***. Una volta pescata, viene aggiunta automaticamente alla scheda del superstite. Essa
 rimarrà segreta fino a che il player non decide di mostrarla **al gruppo** (se la mostra solo a
 un altro player rimarrà nascosta). Nel caso il player pescasse un numero di carte trauma superiore al
limite, il gruppo viene avvisato che il player è destinato a impazzire (il **GM** dovrà creare un PNG
che lo rappresenti).

![draw-trauma-1](resources/tutorial-screenshots/draw_trauma_1.png)

![draw-trauma-2](resources/tutorial-screenshots/draw_trauma_2.png)
![draw-trauma-3](resources/tutorial-screenshots/draw_trauma_3.png)

### 22. /draw_survivor

Il comando `/draw_survivor` viene utilizzato dai **Players** per pescare una carta dal mazzo superstiti. Per pescare,
è necessario passare come parametro il nome di una statistica del player come segue: `/draw_survivor Anima` *(in questo
caso, se il player avesse avuto 3 in Anima allora avrebbe pescato 3 carte dal mazzo superstiti)*. É possibile inoltre 
utilizzare un modificatore che aumenta o diminuisce il numero di carte pescate: `/draw_survivor Anima +1` o 
`/draw_survivor Anima -2`. Se la sintassi non viene rispettata o se vengono usati modificatori non validi apparirà un messaggio
di errore.

![draw-survivor-1](resources/tutorial-screenshots/draw_survivor_1.png)
![draw-survivor-2](resources/tutorial-screenshots/draw_survivor_2.png)

![draw-survivor-3](resources/tutorial-screenshots/draw_survivor_3.png)
![draw-survivor-4](resources/tutorial-screenshots/draw_survivor_4.png)

![draw-survivor-5](resources/tutorial-screenshots/draw_survivor_5.png)
![draw-survivor-6](resources/tutorial-screenshots/draw_survivor_6.png)

### 23. /draw_zombie

Il comando `/draw_zombie` viene utilizzato dai **Players** per pescare una carta dal mazzo zombie (chiamato anche mazzo morso).
Per pescare una sola carta basta chiamare il comando come segue: `/draw_zombie`. É possibile inoltre 
utilizzare un modificatore che indica il numero di carte pescate: `/draw_zombie +1` *(non è pesca 1+1 carte, ma pesca 1 carta)*. 
Se la sintassi non viene rispettata o se vengono usati modificatori non validi apparirà un messaggio di errore.

![draw-zombie-1](resources/tutorial-screenshots/draw_zombie_1.png)
![draw-zombie-2](resources/tutorial-screenshots/draw_zombie_2.png)

![draw-zombie-3](resources/tutorial-screenshots/draw_zombie_3.png)
![draw-zombie-4](resources/tutorial-screenshots/draw_zombie_4.png)

### 24. /draw_fate

Il comando `/draw_fate` viene utilizzato dai **Players** per pescare una carta dal mazzo fato.
Per pescare una sola carta basta chiamare il comando come segue: `/draw_fate`. É possibile inoltre 
utilizzare un modificatore che indica il numero di carte pescate: `/draw_fate +1` *(non è pesca 1+1 carte, ma pesca 1 carta)*. 
Se la sintassi non viene rispettata o se vengono usati modificatori non validi apparirà un messaggio di errore.

![draw-fate-1](resources/tutorial-screenshots/draw_fate_1.png)
![draw-fate-2](resources/tutorial-screenshots/draw_fate_2.png)

![draw-fate-3](resources/tutorial-screenshots/draw_fate_3.png)
![draw-fate-4](resources/tutorial-screenshots/draw_fate_4.png)

### 25. /increase_stress

Il comando `/increase_stress` viene utilizzato dai **Players** per aumentare lo stress del proprio superstite. Una volta
raggiunto il limite esso viene resettato a 0 e il giocatore viene notificato della necessità di pescare una carta trauma
in ***privato***.

![increase-stress-1](resources/tutorial-screenshots/increase_stress_1.png)
![increase-stress-2](resources/tutorial-screenshots/increase_stress_2.png)

### 26. /decrease_stress

Il comando `/decrease_stress` viene utilizzato dai **Players** per diminuire lo stress del proprio superstite. Una volta
raggiunto il livello di stress 0 il player viene notificato che non è più possibile ridurre lo stress.

![decrease-stress-1](resources/tutorial-screenshots/decrease_stress_1.png)
![decrease-stress-2](resources/tutorial-screenshots/decrease_stress_2.png)

### 27. /increase_png_stress

Il comando `/increase_png_stress *png_name*` viene utilizzato dal **GM** per aumentare lo stress dei PNG. 
Il nome del PNG può consistere nelle sole iniziali, non case-sensitive *(deve però rispettare il limite minimo 
di 3 caratteri)*.

![increase-png-stress-1](resources/tutorial-screenshots/increase_png_stress_1.png)
![increase-png-stress-2](resources/tutorial-screenshots/increase_png_stress_2.png)

![increase-png-stress-3](resources/tutorial-screenshots/increase_png_stress_3.png)
![increase-png-stress-4](resources/tutorial-screenshots/increase_png_stress_4.png)

### 28. /show_rel

Il comando `/show_rel` può essere utilizzato in due modalità: 

- La prima consiste nel chiamare il comando senza alcun parametro (`/show_rel`). Ciò è possibile solo se si possiede un survivor
vivo e verranno mostrate le relazioni del survivor del player chiamante.

![show-rel-1](resources/tutorial-screenshots/show_rel_1.png)
![show-rel-2](resources/tutorial-screenshots/show_rel_2.png)

- La seconda invece è utilizzabile anche dal gm e prevede il passaggio di un player come parametro (`/show_rel @player`).
In questo caso verranno mostrati i rapporti del survivor del player passato.

![show-rel-3](resources/tutorial-screenshots/show_rel_3.png)
![show-rel-4](resources/tutorial-screenshots/show_rel_4.png)

### 29. /show_pngs

Il comando `/show_pngs` è molto semplice e mostra semplicemente la lista dei nomi dei ***PNG*** che partecipano a
una campagna.

![show-pngs](resources/tutorial-screenshots/show_pngs.png)

### 30. /show_png

Il comando `/show_png *png_name*` mostra la scheda del ***PNG***. Il nome del PNG può consistere nelle sole iniziali, 
non case-sensitive *(deve però rispettare il limite minimo di 3 caratteri)*.

![show-png-1](resources/tutorial-screenshots/show_png_1.png)
![show-png-2](resources/tutorial-screenshots/show_png_2.png)

![show-png-3](resources/tutorial-screenshots/show_png_3.png)

### 31. /show_enclave

Il comando `/show_enclave` mostra la scheda dell'enclave e le relative immagini dei vari componenti.

![show-enclave-1](resources/tutorial-screenshots/show_enclave_1.png)
![show-enclave-2](resources/tutorial-screenshots/show_enclave_2.png)
![show-enclave-3](resources/tutorial-screenshots/show_enclave_3.png)

### 32. /show_survivor

Il comando `/show_survivor` viene utilizzato dai **Players** per mostrare la scheda del superstite nel gruppo. Di base, 
il *Passato* e le carte *Trauma* sono nascoste fino a quando non sono state rivelate al gruppo **(diverso da rivelarli
a un singolo giocatore)**.

![show-survivor-1](resources/tutorial-screenshots/show_survivor_1.png)
![show-survivor-2](resources/tutorial-screenshots/show_survivor_2.png)

### 33. /show_trauma

Il comando `/show_trauma` può essere utilizzato in due modalità: 

- La prima consiste nel chiamare il comando senza alcun parametro (`/show_trauma`). Ciò è possibile solo se si possiede un survivor
vivo e verranno rivelate le carte trauma **all'intero gruppo**. Saranno dunque visibili a tutti successivamente quando lo 
stesso giocatore chiamerà il comando `/show_survivor`.

![show-trauma-1](resources/tutorial-screenshots/show_trauma_1.png)

- La seconda invece prevede il passaggio di un player come parametro (`/show_trauma @player`).
In questo caso verranno mostrate le carte trauma solo al player interessato mentre il gruppo viene notificato.

![show-trauma-3](resources/tutorial-screenshots/show_trauma_3.png)
![show-trauma-4](resources/tutorial-screenshots/show_trauma_4.png)

### 34. /show_past

Il comando `/show_past` può essere utilizzato in due modalità: 

- La prima consiste nel chiamare il comando senza alcun parametro (`/show_past`). Ciò è possibile solo se si possiede un survivor
vivo e verrà rivelata la carta trauma **all'intero gruppo**. Sarà dunque visibile a tutti successivamente quando lo 
stesso giocatore chiamerà il comando `/show_survivor`.

![show-past-1](resources/tutorial-screenshots/show_past_1.png)

- La seconda invece prevede il passaggio di un player come parametro (`/show_past @player`).
In questo caso verrà mostrata la carta passato solo al player interessato mentre il gruppo viene notificato.

![show-past-2](resources/tutorial-screenshots/show_past_2.png)
![show-past-3](resources/tutorial-screenshots/show_past_3.png)

### 35. /add_ally

Il comando `/add_ally` viene utilizzato dai **Players** per aggiungere un *PNG* come alleato. Ogni *PNG* contiene
una lista di player alleati, visibile nel momento in cui si effettua il comando `/show_png`.

![add-ally-1](resources/tutorial-screenshots/add_ally_1.png)
![add-ally-2](resources/tutorial-screenshots/add_ally_2.png)

### 36. /kill

Il comando `/kill` viene utilizzato dal **GM** per uccidere un superstite. Ottiene lo stesso effetto di `/delete_survivor`,
cancellando dunque **DEFINITIVAMENTE** il survivor e tutto ciò a esso collegato (come i rapporti con altri superstiti o 
le alleanze).

![kill-1](resources/tutorial-screenshots/kill_1.png)
![kill-2](resources/tutorial-screenshots/kill_2.png)

### 37. /base_moves

Il comando `/base_moves` è un comando di aiuto usato dai **Players** o dal **GM** per mostrare la plancia di gioco 
che mostra le mosse base.

![base-moves](resources/tutorial-screenshots/base_moves.png)

### 38. /zombie_moves

Il comando `/zombie_moves` è un comando di aiuto usato dai **Players** o dal **GM** per mostrare la plancia di gioco 
che mostra le mosse zombie.

![base-moves](resources/tutorial-screenshots/zombie_moves.png)

### 39. /gm_moves

Il comando `/gm_moves` è un comando di aiuto usato dal **GM** per mostrare la plancia di gioco che mostra le mosse 
principali del **GM** e da consigli sulla gestione della campagna.

![gm-moves](resources/tutorial-screenshots/zombie_moves.png)

### 40. /enclave_helper

Il comando `/enclave_helper` è un comando di aiuto usato dai **Players** o dal **GM** in **privato** per mostrare la plancia 
di gioco dell'enclave che mostra una breve descrizione di come *cercare provviste* e *spendere materiale*.

![base-moves](resources/tutorial-screenshots/enclave_helper.png)

### 41. /survivor_helper

Il comando `/survivor_helper` è un comando di aiuto usato dai **Players** o dal **GM** in **privato** per mostrare la plancia 
di gioco del superstite che mostra una breve descrizione delle conseguenze di *subire danni gravi* e di come funziona
un'alleanza con un **PNG**.

![base-moves](resources/tutorial-screenshots/survivor_helper.png)

### 42. /help

Il comando `/help` viene utilizzato dagli utenti quando necessitano di chiarimenti. Riporta direttamente a 
[***questa pagina***](TUTORIAL.md#comandi).

![base-moves](resources/tutorial-screenshots/help.png)