FROM python:3.8-alpine

WORKDIR /app

RUN /usr/local/bin/python -m pip install --upgrade pip

RUN apk add --no-cache gcc musl-dev linux-headers

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

EXPOSE 5000

CMD ["python3", "src/main.py"]