# Python Telegram bot Zombieworld

For a complete tutorial on all the commands available please refer to the [Tutorial](TUTORIAL.md) file.

# Env variables setup

Copy `bot.env.template` to `bot.env` and replace with correct parameters

# How to start bot using Docker

__IMPORTANT__ Docker must be installed on your System and running

To start the bot use the following command
```bash
docker compose up -d
```

To also build project after changes use the following command
```bash
docker compose stop
```

```bash
docker compose up -d --build
```
(This command will stop and rebuild container if detects changes)

## Setup Linux (dev environment)

On linux you have to execute the following commands before continuing

```bash
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt install python3.8 -y
sudo apt-get install python3-pip
sudo apt-get install python3.8-venv

python3.8 -m pip install virtualenv
virtualenv -p python3.8 bot-env

source bot-env/bin/activate

pip3 install -r requirements.txt
```

## Setup MacOS (dev environment)

MacOS has by default python 3.8.9 installed, you just need the virtual environment

```bash
pip3 install virtualenv

virtualenv -p python3 bot-env

source bot-env/bin/activate

pip3 install -r requirements.txt
```

## Setup Windows (dev environment)

Download and install python 3.8 from [Python download page](https://www.python.org/downloads/)

```bash
python3 -m pip install virtualenv

virtualenv -p python3 bot-env

./bot-env/Scripts/activate
```

## Environment

To deactivate the virtualenv execute the following command

### On Linux/MacOS
```bash
deactivate
```

### On Windows
```bash
./bot-env/Scripts/deactivate
```

## VSCode setup (Every OS)
If you are using VSCode, after you have configured the venv, and installed `Python extension for VSCode` (by Microsoft), let's configure the editor.
- Press `Control + Shift + P` and insert `Python: select interpreter`
- Select `Python 3.8.* ('bot-env': venv)` interpreter
- Restart the editor

## How to start Bot (Debug mode) using WebHook
You have to install [Ngrok](https://ngrok.com/download), works on every OS.

To start Ngrok run the following command
```bash
ngrok http <FLASK.PORT>
```
Then copy the `https` address given by Ngrok and set it inside `FLASK.HOST`

## Group
- [Razvan Irinel Sarbu](razvanirinel.sarbu@mail.polimi.it)
- [Alberto Rigamonti](alberto1.rigamonti@mail.polimi.it)

## Tutor

- Prof. Giovanni Agosta
